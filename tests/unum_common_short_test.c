/*
 * Includes test case for x2u function.
 * Compile with gcc -lm -lunum -c unum_common_short_test.c
 */
#include "unum_test.h"
#include <libunum.h>

void test_x2u_all_envs()
{
	test_x2u_0_0();
	test_x2u_0_1();
	test_x2u_1_0();
	test_x2u_1_1();
	test_x2u_1_2();
	test_x2u_2_1();
	test_x2u_2_2();
	test_x2u_3_6();
}

void test_x2u_3_6() {
    char actual[1000];
    unum_t u;

    //set environment
    set_env(3, 6);

x2u(-6.44255E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111001100010110000000001001011100100101110110100000") == 0);
x2u(-6.54918E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100000010100111010010011111010101111110011110110100110") == 0);
x2u(-2.20018E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110111010010000001101011101110101110111111010110100100") == 0);
x2u(3.22373E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01110010011011100111111011100100001100000000111010110101000") == 0);
x2u(5.09925E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100101110110101110011110011000000011010110011111") == 0);
//x2u(56742.50833,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111101011101101001101000001000100001111010100011010100101100") == 0);
x2u(56742.50833,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111101011101101001101000001000100001111010100011010110010100110010") == 0);
//x2u(423644399.2,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111011100101000000010011001110111100110011001100110101101011") == 0);
x2u(423644399.2,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011101110010100000001001100111011110011001100110011001100110101110011") == 0);
//x2u(-602968.9543,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111001000100110011010110001111010001001101000000010010101101101") == 0);
x2u(-602968.9543,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111001000100110011010110001111010001001101000000010011101010101110011") == 0);
x2u(-3.56152E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111011110100001111101011000110010111001000110110100011") == 0);
x2u(4.64444E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111001100001000000000010111111101000110000101110110100111") == 0);
x2u(2.91374E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101111000010010000000011001011110001000100110110100101") == 0);
x2u(458910536.5,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111011101101011010011010110100100010101011100") == 0);
x2u(9.17858E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0110110101001101111010100011000000000100100010110100100") == 0);
x2u(-6.72328E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110110011101001001011101101100101001010010110100001") == 0);
x2u(1.47308E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011011100000101111110011101000100000011001110110100011") == 0);
//x2u(-518831198.8,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11110111110111011001011110001011110110011001100110010101101100") == 0);
x2u(-518831198.8,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111101111101110110010111100010111101100110011001100110011010101110011") == 0);
x2u(6.00142E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011100000001000011101001101110010101110011000110110100110") == 0);
x2u(-1.1475E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100011101010110111101000100000101110110011100") == 0);
//x2u(-683855075.1,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11111000100011000010110011001110001100011001100110010101101100") == 0);
x2u(-683855075.1,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111110001000110000101100110011100011000110011001100110011010101110011") == 0);
x2u(-9.94099E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010100010000101010010001001011010111101110110100011") == 0);
x2u(1670485192,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01111011000111001000110010100110010101011010") == 0);
x2u(90787912914,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100011010100100011011000011001110011010010110100010") == 0);
x2u(0,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0000000000000") == 0);
x2u(5.968E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101001010110110110001000100001010010110011100") == 0);
x2u(6.87292E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101100111101000001001000011100011010011110110100010") == 0);
x2u(-14722620390,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100000101101101100010010101101111100110110011111") == 0);
//x2u(-37146057.42,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100000011011011001101110010010110101110000101000110101101101") == 0);
x2u(-37146057.42,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11110000001101101100110111001001011010111000010100011110110101110010") == 0);
x2u(-3247744866,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11111101000001100101001010011101100010101011101") == 0);
x2u(-1.10616E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101010010000011110111101010111101000110110011111") == 0);
//x2u(32643194.79,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0110111111100100001100001111010110010100011110101110101101011") == 0);
x2u(32643194.79,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101111111001000011000011110101100101000111101011100001010101110010") == 0);
x2u(6.84812E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101100111100100100010000101100110011000110110100010") == 0);
x2u(1.23097E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011001110001111010011011100001111001001010110100000") == 0);
//x2u(-29221995.47,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101111011110111100100011010110111100001010001111010101101100") == 0);
x2u(-29221995.47,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111011110111101111001000110101101111000010100011110101110101110000") == 0);
x2u(1.63162E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100111011110111110010000100001011010010110011111") == 0);
x2u(-9.05788E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110011010100101110010100101100000001110110011110") == 0);
x2u(-2.5611E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010000010101000100110101110100101010110110100000") == 0);
x2u(-4.97974E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101111110001001110011110011011011000001001110110100101") == 0);
//x2u(-7631008.768,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010111010001110000101000001100010010011011101001010101101101") == 0);
x2u(-7631008.768,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010111010001110000101000001100010010011011101001011110010101110011") == 0);
x2u(-3.18456E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11110010011010100000101011100100111100100001110110100101") == 0);
x2u(1.56251E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101010110001101100000000010001000111110110110100010") == 0);
x2u(8.00857E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101101001000110101100110011110110011001110010110100101") == 0);
x2u(3.12722E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101011110001110001001000000101111101010010110100010") == 0);
x2u(0,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0000000000000") == 0);
x2u(12123241005,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011000000110100101001100111111010001011010110100000") == 0);
x2u(36134944001,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100010000011010011100111101111101000000010110100010") == 0);
x2u(199551100.5,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011101001111100100111010000111110010101011011") == 0);
x2u(0,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0000000000000") == 0);
x2u(9.43909E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0111000010101101001111010111100001010010100110010110100111") == 0);
//x2u(-527.9019699,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100000000111111100110111001110111111111010110011110100101101") == 0);
x2u(-527.9019699,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110000000011111110011011100111011111111101011001111010100101111") == 0);
x2u(-2.99247E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010111011001101110110001100011101000011110110100011") == 0);
x2u(1.59326E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011001000010100011000100100011101001110110011101") == 0);
//x2u(-418078.9381,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110001100110000100011110111100000010011101010010010101101011") == 0);
x2u(-418078.9381,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100011001100001000111101111000000100111010100100101010010101110010") == 0);
x2u(-9.9783E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101010001000100110100000100000110111001110110100010") == 0);
x2u(-1.80464E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100011001101001010011110111111000100101110110100011") == 0);
x2u(-1.18028E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110011100010010110011100010101110001110110011110") == 0);
x2u(4.18538E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011010001110011100111110000010110100100010110100000") == 0);
x2u(1287790072,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01111010011001100001000011101111110101011010") == 0);
x2u(-2.09844E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100100100001101101110101101010101010110011100") == 0);
x2u(-4.77417E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111011111011001000110101010011101011000111011010110100110") == 0);
x2u(1.17927E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011001110001001010010001111110000010110110110100000") == 0);
x2u(-9.22542E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110110101001111100111100110101101000111101110110100100") == 0);
x2u(-1.88282E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101011000100011111110010000010110000111010110100010") == 0);
x2u(58991147297,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100010101101111000010010101111001001000010110100010") == 0);
x2u(1400943892,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011110101001110000000101101010001010101011011") == 0);
x2u(-8.38415E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101101001100010000001101111011010011001011110110100101") == 0);
//x2u(-1011.258903,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110001111100110100001001000111011101111000110111010100101100") == 0);
x2u(-1011.258903,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100011111001101000010010001110111011110001101110101100010100110011") == 0);
x2u(-2.58614E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101011011110000101010100111000111101110110110100010") == 0);
x2u(-1.77104E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100011001001010110000000110001000010101010110100011") == 0);
x2u(-4.39861E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111011111001000000001101000111101010011111010010110100110") == 0);
x2u(2.77102E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101011100100110011110010001010000010101110110100010") == 0);
x2u(-9.30522E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100001010011100100111000001001010001000000010110100110") == 0);
x2u(-2.52187E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110010011010101101111000010000111100110110011110") == 0);
x2u(1.24021E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101010011010001111001011011000011110101010110100010") == 0);
x2u(2.06487E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0110010010000000100111001010110010011110110011110") == 0);
x2u(-1.27429E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1111000100100001101111010111000010110010100111010110100111") == 0);
x2u(1.66648E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101110001011110010000110000101101011110110110100010") == 0);
//x2u(-26701924.08,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111011110010111011100000110010000010100011110101110101101010") == 0);
x2u(-26701924.08,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110111100101110111000001100100000101000111101011100001010101110001") == 0);
x2u(0,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0000000000000") == 0);
x2u(5.30105E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01101001001101001000111111001100111100001010110100010") == 0);
//x2u(-6376.929182,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101110001110100011101101110111101101111100011110100101010") == 0);
x2u(-6376.929182,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11101110001110100011101101110111101101111100011110000010100110000") == 0);
x2u(6.22482E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011011001100010011101010010011101100101010010110100011") == 0);
x2u(-1.6295E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110111000101000011001111000000110111011011110110100100") == 0);
x2u(3.52306E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011011000000000001010110000110110010111110010110100011") == 0);
x2u(-4412293112,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1111111000001101111111000111111111110101011100") == 0);
x2u(-1.73743E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1111000110001011000010111011110001111110001101110110100111") == 0);
x2u(1.00122E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01110000110001110100110101011001001110001001010110100101") == 0);
x2u(-8.63782E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111100001000100011001101011101011010001011111110110100110") == 0);
x2u(59709751389,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "01100010101111001101111101010000000010111010110100010") == 0);
//x2u(-317359.9143,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111000100110101111010111111101010000011111001000010010101101101") == 0);
x2u(-317359.9143,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111000100110101111010111111101010000011111001000010010110110101110011") == 0);
x2u(2.91145E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011010111010011110101011111101101110001010010110100011") == 0);
x2u(-1.15729E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1111000100000111001000110010010011010110100010010110100111") == 0);
x2u(2.42405E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0110010011000011100000111011011000011010110011110") == 0);
x2u(-15979502148,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110000011011100001110011110110100100010110011110") == 0);
x2u(-8.45282E+13,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "1110110100110011100000110000010101010110100010110100100") == 0);
x2u(1.01648E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0110011011011001010101011101101111010110011011") == 0);
x2u(3.21604E+15,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011100100110110110011111000101010011100100010010110100110") == 0);
x2u(-4.35586E+12,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "111010001111101100010110101111100111011010110100000") == 0);
x2u(6.2955E+14,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "011100000001111001001001010001000000111100100110110100110") == 0);
x2u(0,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "0000000000000") == 0);
x2u(-4.65096E+11,&u);get_unum_string_for_test(actual, u); assert(strcmp(actual, "11100101101100010010011110000111000010110011100") == 0);
printf("All x2u tests for {3,6} passed.\n");
    return;
}

void test_x2u_0_0()
{
    char actual[4], temp[4];
    unum_t u;

    //set environment
    set_env(0, 0);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000") == 0);

    x2u(0.1535, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010") == 0);

    x2u(1.1960, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0011") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111") == 0);

    x2u(-0.8954, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1011") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100") == 0);

    x2u(-3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110") == 0);

    printf("All x2u tests for {0,0} passed.\n");
    return;
}

void test_x2u_0_1()
{
    char actual[6], temp[6];
    unum_t u;

    //set environment
    set_env(0, 1);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100") == 0);

    x2u(100, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000") == 0);

    x2u(-3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100") == 0);

    x2u(-1000.64, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011") == 0);

    x2u(0.3215, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000011") == 0);

    x2u(0.7518, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000111") == 0);

    x2u(1.458, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011") == 0);

    x2u(1.8654, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111") == 0);

    x2u(2.3653, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010011") == 0);

    x2u(2.798, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010111") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111") == 0);

    x2u(-0.1258, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100011") == 0);

    x2u(-0.978, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100111") == 0);

    x2u(-1.489, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011") == 0);

    x2u(-1.745, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111") == 0);

    x2u(-2.426, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110011") == 0);

    x2u(-2.632, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110111") == 0);

    x2u(0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000101") == 0);

    x2u(1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101") == 0);

    x2u(2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010101") == 0);

    x2u(-0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100101") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101") == 0);

    x2u(-2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110101") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101") == 0);

    printf("All x2u tests for {0,1} passed.\n");
    return;
}

void test_x2u_1_0()
{
    char actual[6], temp[6];
    unum_t u;

    //set environment
    set_env(1, 0);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000") == 0);

    x2u(2.7896, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000") == 0);

    x2u(-2.984, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010") == 0);

    x2u(-3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100") == 0);

    x2u(0.2565, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000011") == 0);

    x2u(0.98, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000111") == 0);

    x2u(1.264, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011") == 0);

    x2u(1.79, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111") == 0);

    x2u(3.7685, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010111") == 0);

    x2u(897.65, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111") == 0);

    x2u(-0.2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100011") == 0);

    x2u(-0.6582, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100111") == 0);

    x2u(-1.38, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011") == 0);

    x2u(-1.9622, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111") == 0);

    x2u(-3.2541, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110111") == 0);

    x2u(-32165.64, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011") == 0);

    x2u(0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000101") == 0);

    x2u(1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101") == 0);

    x2u(4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101") == 0);

    x2u(-0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100101") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101") == 0);

    x2u(-4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101") == 0);

	printf("All x2u tests for {1,0} passed.\n");
    return;
}

void test_x2u_1_1()
{
    char actual[8], temp[8];
    unum_t u;

    //set environment
    set_env(1, 1);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000000") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010000") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110000") == 0);

    x2u(-3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000") == 0);

    x2u(2.2539, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100101") == 0);

    x2u(2.6617, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101101") == 0);

    x2u(3.4798, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101") == 0);

    x2u(-2.0204, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100101") == 0);

    x2u(-2.8012, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101101") == 0);

    x2u(-3.1692, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101") == 0);

    x2u(2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101001") == 0);

    x2u(3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001") == 0);

    x2u(-2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101001") == 0);

    x2u(-3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001") == 0);

    x2u(0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010") == 0);

    x2u(1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0011010") == 0);

    x2u(4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010") == 0);

    x2u(6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010") == 0);

    x2u(-0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1011010") == 0);

    x2u(-4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010") == 0);

    x2u(-6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010") == 0);

    x2u(0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011") == 0);

    x2u(0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011") == 0);

    x2u(1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101011") == 0);

    x2u(1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111011") == 0);

    x2u(5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011") == 0);

    x2u(-0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011") == 0);

    x2u(-0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011") == 0);

    x2u(-1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101011") == 0);

    x2u(-1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111011") == 0);

    x2u(-5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011") == 0);

    x2u(0.1745, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111") == 0);

    x2u(0.3275, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111") == 0);

    x2u(0.6368, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111") == 0);

    x2u(0.8923, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111") == 0);

    x2u(1.1569, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111") == 0);

    x2u(1.2548, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101111") == 0);

    x2u(1.6459, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110111") == 0);

    x2u(1.9456, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111111") == 0);

    x2u(3.8201, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011111") == 0);

    x2u(4.4361, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111") == 0);

    x2u(5.8861, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111") == 0);

    x2u(6700.28, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111") == 0);

    x2u(-0.1403, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111") == 0);

    x2u(-0.4859, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111") == 0);

    x2u(-0.594, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111") == 0);

    x2u(-0.9119, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111") == 0);

    x2u(-1.0918, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111") == 0);

    x2u(-1.4256, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101111") == 0);

    x2u(-1.5138, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110111") == 0);

    x2u(-1.8715, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111111") == 0);

    x2u(-3.7125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011111") == 0);

    x2u(-4.927, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111") == 0);

    x2u(-5.6919, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111") == 0);

    x2u(-84.82, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111") == 0);

    printf("All x2u tests for {1,1} passed.\n");
    return;

}

void test_x2u_1_2()
{
    char actual[11], temp[11];
    unum_t u;

    //set environment
    set_env(1, 2);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000000") == 0);

    x2u(0.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000010111") == 0);

    x2u(0.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000010110") == 0);

    x2u(0.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000110111") == 0);

    x2u(0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000010101") == 0);

    x2u(0.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001010111") == 0);

    x2u(0.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000110110") == 0);

    x2u(0.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001110111") == 0);

    x2u(0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010100") == 0);

    x2u(0.5625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010010111") == 0);

    x2u(0.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010110") == 0);

    x2u(0.6875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010110111") == 0);

    x2u(0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000110101") == 0);

    x2u(0.8125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011010111") == 0);

    x2u(0.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110110") == 0);

    x2u(0.9375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011110111") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000") == 0);

    x2u(1.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100010111") == 0);

    x2u(1.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010110") == 0);

    x2u(1.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100110111") == 0);

    x2u(1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010101") == 0);

    x2u(1.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101010111") == 0);

    x2u(1.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010110110") == 0);

    x2u(1.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101110111") == 0);

    x2u(1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110100") == 0);

    x2u(1.5625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110010111") == 0);

    x2u(1.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0011010110") == 0);

    x2u(1.6875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110110111") == 0);

    x2u(1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001110101") == 0);

    x2u(1.8125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111010111") == 0);

    x2u(1.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0011110110") == 0);

    x2u(1.9375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111110111") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100000") == 0);

    x2u(2.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100010011") == 0);

    x2u(2.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010010010") == 0);

    x2u(2.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100110011") == 0);

    x2u(2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010001") == 0);

    x2u(2.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101010011") == 0);

    x2u(2.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010110010") == 0);

    x2u(2.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101110011") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000") == 0);

    x2u(3.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010011") == 0);

    x2u(3.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010010") == 0);

    x2u(3.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110011") == 0);

    x2u(3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001") == 0);

    x2u(3.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010011") == 0);

    x2u(3.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110010") == 0);

    x2u(3.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110011") == 0);

    x2u(4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100") == 0);

    x2u(4.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010111") == 0);

    x2u(4.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010110") == 0);

    x2u(4.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110111") == 0);

    x2u(5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010101") == 0);

    x2u(5.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010111") == 0);

    x2u(5.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110110") == 0);

    x2u(5.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110111") == 0);

    x2u(6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100") == 0);

    x2u(6.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010111") == 0);

    x2u(6.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010110") == 0);

    x2u(6.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110111") == 0);

    x2u(7, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110101") == 0);

    x2u(7.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010111") == 0);

    x2u(7.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110110") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111110111") == 0);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000000") == 0);

    x2u(-0.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000010111") == 0);

    x2u(-0.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000010110") == 0);

    x2u(-0.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000110111") == 0);

    x2u(-0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100010101") == 0);

    x2u(-0.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001010111") == 0);

    x2u(-0.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000110110") == 0);

    x2u(-0.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001110111") == 0);

    x2u(-0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010100") == 0);

    x2u(-0.5625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010010111") == 0);

    x2u(-0.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010110") == 0);

    x2u(-0.6875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010110111") == 0);

    x2u(-0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100110101") == 0);

    x2u(-0.8125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011010111") == 0);

    x2u(-0.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110110") == 0);

    x2u(-0.9375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011110111") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000") == 0);

    x2u(-1.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100010111") == 0);

    x2u(-1.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010110") == 0);

    x2u(-1.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100110111") == 0);

    x2u(-1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010101") == 0);

    x2u(-1.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101010111") == 0);

    x2u(-1.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010110110") == 0);

    x2u(-1.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101110111") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110100") == 0);

    x2u(-1.5625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110010111") == 0);

    x2u(-1.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1011010110") == 0);

    x2u(-1.6875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110110111") == 0);

    x2u(-1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101110101") == 0);

    x2u(-1.8125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111010111") == 0);

    x2u(-1.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1011110110") == 0);

    x2u(-1.9375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111110111") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100000") == 0);

    x2u(-2.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100010011") == 0);

    x2u(-2.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110010010") == 0);

    x2u(-2.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100110011") == 0);

    x2u(-2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010001") == 0);

    x2u(-2.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101010011") == 0);

    x2u(-2.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110110010") == 0);

    x2u(-2.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101110011") == 0);

    x2u(-3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000") == 0);

    x2u(-3.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010011") == 0);

    x2u(-3.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010010") == 0);

    x2u(-3.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110011") == 0);

    x2u(-3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001") == 0);

    x2u(-3.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010011") == 0);

    x2u(-3.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110010") == 0);

    x2u(-3.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110011") == 0);

    x2u(-4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100") == 0);

    x2u(-4.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010111") == 0);

    x2u(-4.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010110") == 0);

    x2u(-4.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110111") == 0);

    x2u(-5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010101") == 0);

    x2u(-5.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010111") == 0);

    x2u(-5.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110110") == 0);

    x2u(-5.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110111") == 0);

    x2u(-6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100") == 0);

    x2u(-6.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010111") == 0);

    x2u(-6.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010110") == 0);

    x2u(-6.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110111") == 0);

    x2u(-7, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110101") == 0);

    x2u(-7.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010111") == 0);

    x2u(-7.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110110") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111110111") == 0);

    x2u(0.028492819, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000001111") == 0);

    x2u(0.104809921, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000011111") == 0);

    x2u(0.138352775, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101111") == 0);

    x2u(0.239627862, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111111") == 0);

    x2u(0.269791746, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001111") == 0);

    x2u(0.349307597, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011111") == 0);

    x2u(0.406189628, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101111") == 0);

    x2u(0.440317403, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111111") == 0);

    x2u(0.542392832, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001111") == 0);

    x2u(0.590401303, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010011111") == 0);

    x2u(0.662708374, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101111") == 0);

    x2u(0.745403235, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111111") == 0);

    x2u(0.778373145, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001111") == 0);

    x2u(0.827663322, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011111") == 0);

    x2u(0.881646871, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101111") == 0);

    x2u(0.964913383, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111111") == 0);

    x2u(1.054400919, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001111") == 0);

    x2u(1.070699381, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100011111") == 0);

    x2u(1.125306837, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101111") == 0);

    x2u(1.228180673, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111111") == 0);

    x2u(1.294229734, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101001111") == 0);

    x2u(1.313820759, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101011111") == 0);

    x2u(1.427286713, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101101111") == 0);

    x2u(1.491632289, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101111111") == 0);

    x2u(1.531831372, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110001111") == 0);

    x2u(1.608487693, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110011111") == 0);

    x2u(1.629253896, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110101111") == 0);

    x2u(1.699523002, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110111111") == 0);

    x2u(1.795820301, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111001111") == 0);

    x2u(1.870310197, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111011111") == 0);

    x2u(1.917164772, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111101111") == 0);

    x2u(1.963577869, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111111111") == 0);

    x2u(2.108575585, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100001011") == 0);

    x2u(2.190115792, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100011011") == 0);

    x2u(2.3137294, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100101011") == 0);

    x2u(2.492162872, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100111011") == 0);

    x2u(2.566171896, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101001011") == 0);

    x2u(2.726517212, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101011011") == 0);

    x2u(2.824619974, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101101011") == 0);

    x2u(2.969395545, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101111011") == 0);

    x2u(3.018232561, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001011") == 0);

    x2u(3.186913291, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011011") == 0);

    x2u(3.267046853, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101011") == 0);

    x2u(3.396629262, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111011") == 0);

    x2u(3.59689405, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001011") == 0);

    x2u(3.670972786, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011011") == 0);

    x2u(3.799682318, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101011") == 0);

    x2u(3.982771918, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011111111") == 0);

    x2u(4.047069934, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001111") == 0);

    x2u(4.2874929, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100011111") == 0);

    x2u(4.520990859, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101111") == 0);

    x2u(4.752554649, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111111") == 0);

    x2u(5.04441203, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001111") == 0);

    x2u(5.302837463, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011111") == 0);

    x2u(5.667381462, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101111") == 0);

    x2u(5.943555415, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111111") == 0);

    x2u(6.185040844, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001111") == 0);

    x2u(6.295907578, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110011111") == 0);

    x2u(6.644321218, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101111") == 0);

    x2u(6.955251044, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111111") == 0);

    x2u(7.170870021, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001111") == 0);

    x2u(7.276925477, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011111") == 0);

    x2u(23750853.8, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101111") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111111") == 0);

    x2u(-0.016535738, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000001111") == 0);

    x2u(-0.099982022, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000011111") == 0);

    x2u(-0.17731373, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101111") == 0);

    x2u(-0.245672752, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111111") == 0);

    x2u(-0.253667332, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001111") == 0);

    x2u(-0.343585054, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011111") == 0);

    x2u(-0.402789474, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101111") == 0);

    x2u(-0.478885392, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111111") == 0);

    x2u(-0.50748917, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001111") == 0);

    x2u(-0.604005825, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010011111") == 0);

    x2u(-0.63727916, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101111") == 0);

    x2u(-0.7394548, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111111") == 0);

    x2u(-0.777945635, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001111") == 0);

    x2u(-0.844016436, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011111") == 0);

    x2u(-0.905807958, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101111") == 0);

    x2u(-0.964282052, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111111") == 0);

    x2u(-1.007581484, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001111") == 0);

    x2u(-1.072759592, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100011111") == 0);

    x2u(-1.163332466, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101111") == 0);

    x2u(-1.239345586, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111111") == 0);

    x2u(-1.302556804, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101001111") == 0);

    x2u(-1.317975544, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101011111") == 0);

    x2u(-1.393767706, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101101111") == 0);

    x2u(-1.439308722, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101111111") == 0);

    x2u(-1.559779213, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110001111") == 0);

    x2u(-1.579305814, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110011111") == 0);

    x2u(-1.677253686, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110101111") == 0);

    x2u(-1.72445525, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110111111") == 0);

    x2u(-1.779208509, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111001111") == 0);

    x2u(-1.841625875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111011111") == 0);

    x2u(-1.882443339, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111101111") == 0);

    x2u(-1.973296305, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111111111") == 0);

    x2u(-2.006420495, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100001011") == 0);

    x2u(-2.131625995, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100011011") == 0);

    x2u(-2.269830417, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100101011") == 0);

    x2u(-2.454659773, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100111011") == 0);

    x2u(-2.602801005, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101001011") == 0);

    x2u(-2.72953269, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101011011") == 0);

    x2u(-2.854392626, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101101011") == 0);

    x2u(-2.960985302, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101111011") == 0);

    x2u(-3.029410957, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001011") == 0);

    x2u(-3.140084633, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011011") == 0);

    x2u(-3.365201405, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101011") == 0);

    x2u(-3.388069426, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111011") == 0);

    x2u(-3.591701215, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001011") == 0);

    x2u(-3.681570247, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011011") == 0);

    x2u(-3.855005157, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101011") == 0);

    x2u(-3.97878589, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011111111") == 0);

    x2u(-4.082839636, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001111") == 0);

    x2u(-4.403337296, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100011111") == 0);

    x2u(-4.578909264, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101111") == 0);

    x2u(-4.981043406, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111111") == 0);

    x2u(-5.066888917, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001111") == 0);

    x2u(-5.275806364, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011111") == 0);

    x2u(-5.641824993, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101111") == 0);

    x2u(-5.911538116, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111111") == 0);

    x2u(-6.143906815, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001111") == 0);

    x2u(-6.438861252, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110011111") == 0);

    x2u(-6.720126795, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101111") == 0);

    x2u(-6.812288168, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111111") == 0);

    x2u(-7.233493476, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001111") == 0);

    x2u(-7.471709206, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011111") == 0);

    x2u(-1283.677691, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101111") == 0);

    printf("All x2u tests for {1,2} passed.\n");
    return;
}

void test_x2u_2_1()
{
    char actual[11], temp[11];
    unum_t u;

    //set environment
    set_env(2, 1);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000000") == 0);

    x2u(0.00390625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000010111") == 0);

    x2u(0.0078125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000010110") == 0);

    x2u(0.01171875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000110111") == 0);

    x2u(0.015625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000100110") == 0);

    x2u(0.01953125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001010111") == 0);

    x2u(0.0234375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000110110") == 0);

    x2u(0.02734375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001110111") == 0);

    x2u(0.03125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001000110") == 0);

    x2u(0.0390625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010010111") == 0);

    x2u(0.046875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010110") == 0);

    x2u(0.0546875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010110111") == 0);

    x2u(0.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001100110") == 0);

    x2u(0.078125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011010111") == 0);

    x2u(0.09375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110110") == 0);

    x2u(0.109375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011110111") == 0);

    x2u(0.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000110") == 0);

    x2u(0.15625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100010111") == 0);

    x2u(0.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010110") == 0);

    x2u(0.21875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100110111") == 0);

    x2u(0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000100100") == 0);

    x2u(0.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010101") == 0);

    x2u(0.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000110100") == 0);

    x2u(0.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110101") == 0);

    x2u(0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000100") == 0);

    x2u(0.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010101") == 0);

    x2u(0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010100") == 0);

    x2u(0.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010110101") == 0);

    x2u(1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000") == 0);

    x2u(1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010011") == 0);

    x2u(1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110010") == 0);

    x2u(1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001110011") == 0);

    x2u(2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100000") == 0);

    x2u(2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010001") == 0);

    x2u(3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000") == 0);

    x2u(3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001") == 0);

    x2u(4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010") == 0);

    x2u(5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010011") == 0);

    x2u(6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010") == 0);

    x2u(7, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110011") == 0);

    x2u(8, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000100") == 0);

    x2u(10, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010101") == 0);

    x2u(12, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010100") == 0);

    x2u(14, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110101") == 0);

    x2u(16, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100100") == 0);

    x2u(20, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010101") == 0);

    x2u(24, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110100") == 0);

    x2u(28, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110101") == 0);

    x2u(32, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000110") == 0);

    x2u(40, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010111") == 0);

    x2u(48, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010110") == 0);

    x2u(56, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110111") == 0);

    x2u(64, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100110") == 0);

    x2u(80, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010111") == 0);

    x2u(96, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110110") == 0);

    x2u(112, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110111") == 0);

    x2u(128, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111000110") == 0);

    x2u(160, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010111") == 0);

    x2u(192, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010110") == 0);

    x2u(224, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110111") == 0);

    x2u(256, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100110") == 0);

    x2u(320, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010111") == 0);

    x2u(384, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110110") == 0);

    x2u(INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111110111") == 0);

    x2u(0, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000000") == 0);

    x2u(-0.00390625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000010111") == 0);

    x2u(-0.0078125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000010110") == 0);

    x2u(-0.01171875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000110111") == 0);

    x2u(-0.015625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000100110") == 0);

    x2u(-0.01953125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001010111") == 0);

    x2u(-0.0234375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000110110") == 0);

    x2u(-0.02734375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001110111") == 0);

    x2u(-0.03125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001000110") == 0);

    x2u(-0.0390625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010010111") == 0);

    x2u(-0.046875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010110") == 0);

    x2u(-0.0546875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010110111") == 0);

    x2u(-0.0625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001100110") == 0);

    x2u(-0.078125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011010111") == 0);

    x2u(-0.09375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110110") == 0);

    x2u(-0.109375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011110111") == 0);

    x2u(-0.125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000110") == 0);

    x2u(-0.15625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100010111") == 0);

    x2u(-0.1875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010110") == 0);

    x2u(-0.21875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100110111") == 0);

    x2u(-0.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100100100") == 0);

    x2u(-0.3125, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010101") == 0);

    x2u(-0.375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100110100") == 0);

    x2u(-0.4375, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110101") == 0);

    x2u(-0.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000100") == 0);

    x2u(-0.625, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010101") == 0);

    x2u(-0.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010100") == 0);

    x2u(-0.875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010110101") == 0);

    x2u(-1, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000") == 0);

    x2u(-1.25, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010011") == 0);

    x2u(-1.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110010") == 0);

    x2u(-1.75, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101110011") == 0);

    x2u(-2, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100000") == 0);

    x2u(-2.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010001") == 0);

    x2u(-3, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000") == 0);

    x2u(-3.5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001") == 0);

    x2u(-4, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010") == 0);

    x2u(-5, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010011") == 0);

    x2u(-6, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010") == 0);

    x2u(-7, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110011") == 0);

    x2u(-8, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000100") == 0);

    x2u(-10, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010101") == 0);

    x2u(-12, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010100") == 0);

    x2u(-14, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110101") == 0);

    x2u(-16, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100100") == 0);

    x2u(-20, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010101") == 0);

    x2u(-24, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110100") == 0);

    x2u(-28, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110101") == 0);

    x2u(-32, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000110") == 0);

    x2u(-40, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010111") == 0);

    x2u(-48, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010110") == 0);

    x2u(-56, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110111") == 0);

    x2u(-64, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100110") == 0);

    x2u(-80, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010111") == 0);

    x2u(-96, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110110") == 0);

    x2u(-112, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110111") == 0);

    x2u(-128, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111000110") == 0);

    x2u(-160, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010111") == 0);

    x2u(-192, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010110") == 0);

    x2u(-224, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110111") == 0);

    x2u(-256, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100110") == 0);

    x2u(-320, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010111") == 0);

    x2u(-384, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110110") == 0);

    x2u(-INFINITY, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111110111") == 0);

    x2u(0.002568616, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000001111") == 0);

    x2u(0.004151194, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000011111") == 0);

    x2u(0.011488765, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101111") == 0);

    x2u(0.012215632, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111111") == 0);

    x2u(0.016025095, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001111") == 0);

    x2u(0.021567117, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011111") == 0);

    x2u(0.02676966, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101111") == 0);

    x2u(0.027760421, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111111") == 0);

    x2u(0.035859174, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001111") == 0);

    x2u(0.04173568, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010011111") == 0);

    x2u(0.053758929, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101111") == 0);

    x2u(0.055876123, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111111") == 0);

    x2u(0.071350213, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001111") == 0);

    x2u(0.089341192, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011111") == 0);

    x2u(0.094777163, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101111") == 0);

    x2u(0.115886121, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111111") == 0);

    x2u(0.140565287, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001111") == 0);

    x2u(0.168309636, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100011111") == 0);

    x2u(0.214932761, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101111") == 0);

    x2u(0.246509319, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111111") == 0);

    x2u(0.28048339, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001101") == 0);

    x2u(0.3262694, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001011101") == 0);

    x2u(0.42649235, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101101") == 0);

    x2u(0.450704745, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001111101") == 0);

    x2u(0.617878983, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001101") == 0);

    x2u(0.713272771, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010011101") == 0);

    x2u(0.807180247, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010101101") == 0);

    x2u(0.910436268, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010111101") == 0);

    x2u(1.157855742, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001011") == 0);

    x2u(1.36258193, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011011") == 0);

    x2u(1.73863268, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101011") == 0);

    x2u(1.943207944, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111011") == 0);

    x2u(2.466424117, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01001001") == 0);

    x2u(2.904559798, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011001") == 0);

    x2u(3.121240506, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001") == 0);

    x2u(3.915370655, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010111011") == 0);

    x2u(4.901862477, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001011") == 0);

    x2u(5.764542461, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011011") == 0);

    x2u(6.711109936, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101011") == 0);

    x2u(7.399913401, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101111101") == 0);

    x2u(9.052413188, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001101") == 0);

    x2u(11.98663175, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011101") == 0);

    x2u(13.98171088, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101101") == 0);

    x2u(14.31753902, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111101") == 0);

    x2u(17.98463458, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001101") == 0);

    x2u(23.46149311, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011101") == 0);

    x2u(25.07501528, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101101") == 0);

    x2u(29.90742346, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011111111") == 0);

    x2u(37.72448346, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001111") == 0);

    x2u(42.31491095, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100011111") == 0);

    x2u(50.5341189, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101111") == 0);

    x2u(59.20444983, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111111") == 0);

    x2u(78.12229468, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001111") == 0);

    x2u(95.61241931, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011111") == 0);

    x2u(107.5343935, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101111") == 0);

    x2u(122.1421614, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111111") == 0);

    x2u(143.1304512, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001111") == 0);

    x2u(172.50633, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110011111") == 0);

    x2u(195.7895829, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101111") == 0);

    x2u(242.3118531, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111111") == 0);

    x2u(308.9691593, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001111") == 0);

    x2u(323.7894567, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011111") == 0);

    x2u(18560.79011, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101111") == 0);

    x2u(NAN, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111111") == 0);

    x2u(-0.000935681, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000001111") == 0);

    x2u(-0.004843712, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000011111") == 0);

    x2u(-0.00997149, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101111") == 0);

    x2u(-0.01343178, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111111") == 0);

    x2u(-0.017603089, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001111") == 0);

    x2u(-0.022209108, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011111") == 0);

    x2u(-0.025548549, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101111") == 0);

    x2u(-0.030277535, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111111") == 0);

    x2u(-0.038547181, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001111") == 0);

    x2u(-0.040890636, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010011111") == 0);

    x2u(-0.047046166, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101111") == 0);

    x2u(-0.059356112, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111111") == 0);

    x2u(-0.066577245, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001111") == 0);

    x2u(-0.084898802, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011111") == 0);

    x2u(-0.104602407, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101111") == 0);

    x2u(-0.120704731, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111111") == 0);

    x2u(-0.130848875, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001111") == 0);

    x2u(-0.179436162, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100011111") == 0);

    x2u(-0.189365777, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101111") == 0);

    x2u(-0.242789372, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111111") == 0);

    x2u(-0.258315687, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001101") == 0);

    x2u(-0.340795972, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001011101") == 0);

    x2u(-0.375159898, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101101") == 0);

    x2u(-0.447852226, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001111101") == 0);

    x2u(-0.596356475, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001101") == 0);

    x2u(-0.629082958, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010011101") == 0);

    x2u(-0.768550224, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010101101") == 0);

    x2u(-0.947524071, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010111101") == 0);

    x2u(-1.044784586, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001011") == 0);

    x2u(-1.334349976, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011011") == 0);

    x2u(-1.634414805, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101011") == 0);

    x2u(-1.942538345, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111011") == 0);

    x2u(-2.299778227, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11001001") == 0);

    x2u(-2.697361333, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011001") == 0);

    x2u(-3.042335372, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001") == 0);

    x2u(-3.803953688, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110111011") == 0);

    x2u(-4.218523619, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001011") == 0);

    x2u(-5.429754743, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011011") == 0);

    x2u(-6.045165186, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101011") == 0);

    x2u(-7.880734942, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101111101") == 0);

    x2u(-9.939016497, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001101") == 0);

    x2u(-10.40301373, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011101") == 0);

    x2u(-13.89806374, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101101") == 0);

    x2u(-15.37505165, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111101") == 0);

    x2u(-18.44680692, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001101") == 0);

    x2u(-20.73815674, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011101") == 0);

    x2u(-26.18407466, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101101") == 0);

    x2u(-30.19965511, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011111111") == 0);

    x2u(-35.53708, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001111") == 0);

    x2u(-44.37022526, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100011111") == 0);

    x2u(-54.3141459, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101111") == 0);

    x2u(-58.6118622, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111111") == 0);

    x2u(-64.81974817, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001111") == 0);

    x2u(-90.20836982, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011111") == 0);

    x2u(-103.7749564, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101111") == 0);

    x2u(-124.1080392, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111111") == 0);

    x2u(-135.1681288, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001111") == 0);

    x2u(-190.6069127, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110011111") == 0);

    x2u(-210.424358, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101111") == 0);

    x2u(-240.7014077, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111111") == 0);

    x2u(-263.2428997, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001111") == 0);

    x2u(-374.9090248, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011111") == 0);

    x2u(-299974.6896, &u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101111") == 0);

    printf("All x2u tests for {2,1} passed.\n");
	return;
}

void test_x2u_2_2()
{
    char actual[14], temp[14];
    unum_t u;

    //set environment
    set_env(2, 2);


    x2u(0 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000000") == 0);

    x2u(0.000976563 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000000111111") == 0);

    x2u(0.001953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000000101110") == 0);

    x2u(0.002929688 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000001111111") == 0);

    x2u(0.00390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000000101101") == 0);

    x2u(0.004882813 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000010111111") == 0);

    x2u(0.005859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000001101110") == 0);

    x2u(0.006835938 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000011111111") == 0);

    x2u(0.0078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101100") == 0);


    x2u(0.008789063 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000100111111") == 0);

    x2u(0.009765625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000010101110") == 0);

    x2u(0.010742188 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101111111") == 0);

    x2u(0.01171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000001101101") == 0);

    x2u(0.012695313 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000110111111") == 0);

    x2u(0.013671875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000011101110") == 0);

    x2u(0.014648438 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111111111") == 0);

    x2u(0.015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001100") == 0);

    x2u(0.01660157 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001000111111") == 0);


    x2u(0.017578125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000100101110") == 0);

    x2u(0.018554788 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001111111") == 0);

    x2u(0.01953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000010101101") == 0);

    x2u(0.02051 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001010111111") == 0);

    x2u(0.021484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000101101110") == 0);

    x2u(0.022461 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011111111") == 0);

    x2u(0.0234375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101100") == 0);


    x2u(0.0245 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001100111111") == 0);

    x2u(0.025390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000110101110") == 0);

    x2u(0.0264 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101111111") == 0);

    x2u(0.02734375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000011101101") == 0);

    x2u(0.0284 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001110111111") == 0);

    x2u(0.029296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0000111101110") == 0);

    x2u(0.0303 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111111111") == 0);

    x2u(0.03125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001100") == 0);

    x2u(0.033203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010000101111") == 0);

    x2u(0.03515625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001000101110") == 0);


    x2u(0.037109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001101111") == 0);

    x2u(0.0390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000100101101") == 0);

    x2u(0.041015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010010101111") == 0);

    x2u(0.04296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001101110") == 0);

    x2u(0.044921875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010011101111") == 0);

    x2u(0.046875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101100") == 0);

    x2u(0.048828125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010100101111") == 0);

    x2u(0.05078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010101110") == 0);

    x2u(0.052734375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101101111") == 0);

    x2u(0.0546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000101101101") == 0);

    x2u(0.056640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010110101111") == 0);

    x2u(0.05859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001011101110") == 0);

    x2u(0.060546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111101111") == 0);

    x2u(0.0625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001100") == 0);

    x2u(0.06640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011000101111") == 0);

    x2u(0.0703125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001100101110") == 0);

    x2u(0.07421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001101111") == 0);

    x2u(0.078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000110101101") == 0);

    x2u(0.08203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011010101111") == 0);

    x2u(0.0859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101101110") == 0);

    x2u(0.08984375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011101111") == 0);

    x2u(0.09375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101100") == 0);

    x2u(0.09765625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011100101111") == 0);

    x2u(0.1015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110101110") == 0);

    x2u(0.10546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101101111") == 0);

    x2u(0.109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000111101101") == 0);

    x2u(0.11328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011110101111") == 0);

    x2u(0.1171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001111101110") == 0);

    x2u(0.12109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111101111") == 0);

    x2u(0.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001100") == 0);

    x2u(0.1328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100000101111") == 0);

    x2u(0.140625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000101110") == 0);

    x2u(0.1484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001101111") == 0);

    x2u(0.15625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000101101") == 0);

    x2u(0.1640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100010101111") == 0);

    x2u(0.171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001101110") == 0);

    x2u(0.1796875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100011101111") == 0);

    x2u(0.1875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101100") == 0);

    x2u(0.1953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100100101111") == 0);

    x2u(0.203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010101110") == 0);

    x2u(0.2109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101101111") == 0);

    x2u(0.21875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001101101") == 0);

    x2u(0.2265625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100110101111") == 0);

    x2u(0.234375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010011101110") == 0);

    x2u(0.2421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111101111") == 0);

    x2u(0.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001000") == 0);

    x2u(0.265625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001000101011") == 0);

    x2u(0.28125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000100101010") == 0);

    x2u(0.296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001101011") == 0);

    x2u(0.3125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101001") == 0);

    x2u(0.328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010101011") == 0);

    x2u(0.34375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000101101010") == 0);

    x2u(0.359375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001011101011") == 0);

    x2u(0.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101000") == 0);

    x2u(0.390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001100101011") == 0);

    x2u(0.40625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000110101010") == 0);

    x2u(0.421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101101011") == 0);

    x2u(0.4375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101001") == 0);

    x2u(0.453125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110101011") == 0);

    x2u(0.46875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "000111101010") == 0);

    x2u(0.484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001111101011") == 0);

    x2u(0.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001000") == 0);

    x2u(0.53125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000101011") == 0);

    x2u(0.5625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000101010") == 0);

    x2u(0.59375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001101011") == 0);

    x2u(0.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101001") == 0);

    x2u(0.65625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010101011") == 0);

    x2u(0.6875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001101010") == 0);

    x2u(0.71875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010011101011") == 0);

    x2u(0.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010101000") == 0);

    x2u(0.78125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010100101011") == 0);

    x2u(0.8125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010101010") == 0);

    x2u(0.84375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010101101011") == 0);

    x2u(0.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101101001") == 0);

    x2u(0.90625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010110101011") == 0);

    x2u(0.9375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011101010") == 0);

    x2u(0.96875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010111101011") == 0);

    x2u(1 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100000") == 0);

    x2u(1.0625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000100111") == 0);

    x2u(1.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100100110") == 0);

    x2u(1.1875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001100111") == 0);

    x2u(1.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010100101") == 0);

    x2u(1.3125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010100111") == 0);

    x2u(1.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00101100110") == 0);

    x2u(1.4375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011100111") == 0);

    x2u(1.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001100100") == 0);

    x2u(1.5625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001100100111") == 0);

    x2u(1.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00110100110") == 0);

    x2u(1.6875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101100111") == 0);

    x2u(1.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0011100101") == 0);

    x2u(1.8125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001110100111") == 0);

    x2u(1.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00111100110") == 0);

    x2u(1.9375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111100111") == 0);

    x2u(2 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000000") == 0);

    x2u(2.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000100011") == 0);

    x2u(2.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0100100010") == 0);

    x2u(2.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01001100011") == 0);

    x2u(2.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010100001") == 0);

    x2u(2.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010100011") == 0);

    x2u(2.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101100010") == 0);

    x2u(2.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011100011") == 0);

    x2u(3 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100000") == 0);

    x2u(3.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100011") == 0);

    x2u(3.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100010") == 0);

    x2u(3.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101100011") == 0);

    x2u(3.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100001") == 0);

    x2u(3.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100011") == 0);

    x2u(3.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100010") == 0);

    x2u(3.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111100011") == 0);

    x2u(4 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000100") == 0);

    x2u(4.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000100111") == 0);

    x2u(4.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100110") == 0);

    x2u(4.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001100111") == 0);

    x2u(5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100101") == 0);

    x2u(5.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010100111") == 0);

    x2u(5.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101100110") == 0);

    x2u(5.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011100111") == 0);

    x2u(6 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100100") == 0);

    x2u(6.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100100111") == 0);

    x2u(6.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100110") == 0);

    x2u(6.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101100111") == 0);

    x2u(7 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100101") == 0);

    x2u(7.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110100111") == 0);

    x2u(7.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111100110") == 0);

    x2u(7.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111100111") == 0);

    x2u(8 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001000") == 0);

    x2u(8.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000101011") == 0);

    x2u(9 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000101010") == 0);

    x2u(9.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001101011") == 0);

    x2u(10 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101001") == 0);

    x2u(10.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010101011") == 0);

    x2u(11 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001101010") == 0);

    x2u(11.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011101011") == 0);

    x2u(12 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101000") == 0);

    x2u(12.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100101011") == 0);

    x2u(13 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010101010") == 0);

    x2u(13.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101101011") == 0);

    x2u(14 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101001") == 0);

    x2u(14.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110101011") == 0);

    x2u(15 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011101010") == 0);

    x2u(15.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111101011") == 0);

    x2u(16 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001000") == 0);

    x2u(17 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111000101011") == 0);

    x2u(18 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100101010") == 0);

    x2u(19 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001101011") == 0);

    x2u(20 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101001") == 0);

    x2u(21 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010101011") == 0);

    x2u(22 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101101010") == 0);

    x2u(23 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011101011") == 0);

    x2u(24 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101000") == 0);

    x2u(25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100101011") == 0);

    x2u(26 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110101010") == 0);

    x2u(27 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101101011") == 0);

    x2u(28 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101001") == 0);

    x2u(29 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110101011") == 0);

    x2u(30 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111101010") == 0);

    x2u(31 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111111101011") == 0);

    x2u(32 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001100") == 0);

    x2u(34 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100000101111") == 0);

    x2u(36 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000101110") == 0);

    x2u(38 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001101111") == 0);

    x2u(40 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000101101") == 0);

    x2u(42 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010101111") == 0);

    x2u(44 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001101110") == 0);

    x2u(46 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100011101111") == 0);

    x2u(48 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101100") == 0);

    x2u(50 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100101111") == 0);

    x2u(52 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010101110") == 0);

    x2u(54 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101101111") == 0);

    x2u(56 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001101101") == 0);

    x2u(58 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110101111") == 0);

    x2u(60 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011101110") == 0);

    x2u(62 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111101111") == 0);

    x2u(64 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001100") == 0);

    x2u(68 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101000101111") == 0);

    x2u(72 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100101110") == 0);

    x2u(76 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001101111") == 0);

    x2u(80 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010101101") == 0);

    x2u(84 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010101111") == 0);

    x2u(88 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101101110") == 0);

    x2u(92 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011101111") == 0);

    x2u(96 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101100") == 0);

    x2u(100 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101100101111") == 0);

    x2u(104 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110101110") == 0);

    x2u(108 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101101111") == 0);

    x2u(112 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011101101") == 0);

    x2u(116 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110101111") == 0);

    x2u(120 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111101110") == 0);

    x2u(124 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111101111") == 0);

    x2u(128 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001100") == 0);

    x2u(136 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110000101111") == 0);

    x2u(144 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111000101110") == 0);

    x2u(152 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001101111") == 0);

    x2u(160 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100101101") == 0);

    x2u(168 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010101111") == 0);

    x2u(176 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001101110") == 0);

    x2u(184 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110011101111") == 0);

    x2u(192 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101100") == 0);

    x2u(200 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100101111") == 0);

    x2u(208 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010101110") == 0);

    x2u(216 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101101111") == 0);

    x2u(224 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101101101") == 0);

    x2u(232 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110101111") == 0);

    x2u(240 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011101110") == 0);

    x2u(248 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111101111") == 0);

    x2u(256 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001100") == 0);

    x2u(272 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111000101111") == 0);

    x2u(288 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100101110") == 0);

    x2u(304 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001101111") == 0);

    x2u(320 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110101101") == 0);

    x2u(336 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010101111") == 0);

    x2u(352 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101101110") == 0);

    x2u(368 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011101111") == 0);

    x2u(384 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101100") == 0);

    x2u(400 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111100101111") == 0);

    x2u(416 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110101110") == 0);

    x2u(432 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101101111") == 0);

    x2u(448 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111101101") == 0);

    x2u(464 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111110101111") == 0);

    x2u(480 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111111101110") == 0);

    x2u(INFINITY ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111101111") == 0);

    x2u(0 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000000") == 0);

    x2u(-0.000976563 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000000111111") == 0);

    x2u(-0.001953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000000101110") == 0);

    x2u(-0.002929688 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000001111111") == 0);

    x2u(-0.00390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100000101101") == 0);

    x2u(-0.004882813 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000010111111") == 0);

    x2u(-0.005859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000001101110") == 0);

    x2u(-0.006835938 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000011111111") == 0);

    x2u(-0.0078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101100") == 0);

    x2u(-0.008789063 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000100111111") == 0);

    x2u(-0.009765625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000010101110") == 0);

    x2u(-0.010742188 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101111111") == 0);

    x2u(-0.01171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100001101101") == 0);

    x2u(-0.012695313 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000110111111") == 0);

    x2u(-0.013671875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000011101110") == 0);

    x2u(-0.014648438 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111111111") == 0);

    x2u(-0.015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001100") == 0);

    x2u(-0.0167 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001000111111") == 0);

    x2u(-0.017578125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000100101110") == 0);

    x2u(-0.0190 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001111111") == 0);

    x2u(-0.01953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100010101101") == 0);

    x2u(-0.0206 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001010111111") == 0);

    x2u(-0.021484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000101101110") == 0);

    x2u(-0.0225 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011111111") == 0);

    x2u(-0.0234375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101100") == 0);

    x2u(-0.0245 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001100111111") == 0);

    x2u(-0.025390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000110101110") == 0);

    x2u(-0.0265 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101111111") == 0);

    x2u(-0.02734375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100011101101") == 0);

    x2u(-0.0284 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001110111111") == 0);

    x2u(-0.029296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1000111101110") == 0);

    x2u(-0.03028 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111111111") == 0);

    x2u(-0.03125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001100") == 0);

    x2u(-0.033203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010000101111") == 0);

    x2u(-0.03515625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001000101110") == 0);

    x2u(-0.037109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001101111") == 0);

    x2u(-0.0390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100100101101") == 0);

    x2u(-0.041015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010010101111") == 0);

    x2u(-0.04296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001101110") == 0);

    x2u(-0.044921875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010011101111") == 0);

    x2u(-0.046875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101100") == 0);

    x2u(-0.048828125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010100101111") == 0);

    x2u(-0.05078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010101110") == 0);

    x2u(-0.052734375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101101111") == 0);

    x2u(-0.0546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100101101101") == 0);

    x2u(-0.056640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010110101111") == 0);

    x2u(-0.05859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001011101110") == 0);

    x2u(-0.060546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111101111") == 0);

    x2u(-0.0625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001100") == 0);

    x2u(-0.06640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011000101111") == 0);

    x2u(-0.0703125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001100101110") == 0);

    x2u(-0.07421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001101111") == 0);

    x2u(-0.078125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100110101101") == 0);

    x2u(-0.08203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011010101111") == 0);

    x2u(-0.0859375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101101110") == 0);

    x2u(-0.08984375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011101111") == 0);

    x2u(-0.09375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101100") == 0);

    x2u(-0.09765625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011100101111") == 0);

    x2u(-0.1015625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110101110") == 0);

    x2u(-0.10546875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101101111") == 0);

    x2u(-0.109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100111101101") == 0);

    x2u(-0.11328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011110101111") == 0);

    x2u(-0.1171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001111101110") == 0);

    x2u(-0.12109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111101111") == 0);

    x2u(-0.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001100") == 0);

    x2u(-0.1328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100000101111") == 0);

    x2u(-0.140625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000101110") == 0);

    x2u(-0.1484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001101111") == 0);

    x2u(-0.15625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000101101") == 0);

    x2u(-0.1640625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100010101111") == 0);

    x2u(-0.171875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001101110") == 0);

    x2u(-0.1796875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100011101111") == 0);

    x2u(-0.1875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101100") == 0);

    x2u(-0.1953125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100100101111") == 0);

    x2u(-0.203125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010101110") == 0);

    x2u(-0.2109375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101101111") == 0);

    x2u(-0.21875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001101101") == 0);

    x2u(-0.2265625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100110101111") == 0);

    x2u(-0.234375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010011101110") == 0);

    x2u(-0.2421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111101111") == 0);

    x2u(-0.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001000") == 0);

    x2u(-0.265625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001000101011") == 0);

    x2u(-0.28125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100100101010") == 0);

    x2u(-0.296875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001101011") == 0);

    x2u(-0.3125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101001") == 0);

    x2u(-0.328125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010101011") == 0);

    x2u(-0.34375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100101101010") == 0);

    x2u(-0.359375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001011101011") == 0);

    x2u(-0.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101000") == 0);

    x2u(-0.390625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001100101011") == 0);

    x2u(-0.40625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100110101010") == 0);

    x2u(-0.421875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101101011") == 0);

    x2u(-0.4375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101001") == 0);

    x2u(-0.453125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110101011") == 0);

    x2u(-0.46875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "100111101010") == 0);

    x2u(-0.484375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001111101011") == 0);

    x2u(-0.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001000") == 0);

    x2u(-0.53125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000101011") == 0);

    x2u(-0.5625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000101010") == 0);

    x2u(-0.59375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001101011") == 0);

    x2u(-0.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101001") == 0);

    x2u(-0.65625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010101011") == 0);

    x2u(-0.6875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001101010") == 0);

    x2u(-0.71875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010011101011") == 0);

    x2u(-0.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010101000") == 0);

    x2u(-0.78125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010100101011") == 0);

    x2u(-0.8125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010101010") == 0);

    x2u(-0.84375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010101101011") == 0);

    x2u(-0.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101101001") == 0);

    x2u(-0.90625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010110101011") == 0);

    x2u(-0.9375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011101010") == 0);

    x2u(-0.96875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010111101011") == 0);

    x2u(-1 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100000") == 0);

    x2u(-1.0625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000100111") == 0);

    x2u(-1.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100100110") == 0);

    x2u(-1.1875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001100111") == 0);

    x2u(-1.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010100101") == 0);

    x2u(-1.3125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010100111") == 0);

    x2u(-1.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10101100110") == 0);

    x2u(-1.4375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011100111") == 0);

    x2u(-1.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101100100") == 0);

    x2u(-1.5625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101100100111") == 0);

    x2u(-1.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10110100110") == 0);

    x2u(-1.6875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101100111") == 0);

    x2u(-1.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1011100101") == 0);

    x2u(-1.8125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101110100111") == 0);

    x2u(-1.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10111100110") == 0);

    x2u(-1.9375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111100111") == 0);

    x2u(-2 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000000") == 0);

    x2u(-2.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000100011") == 0);

    x2u(-2.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1100100010") == 0);

    x2u(-2.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11001100011") == 0);

    x2u(-2.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110100001") == 0);

    x2u(-2.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010100011") == 0);

    x2u(-2.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101100010") == 0);

    x2u(-2.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011100011") == 0);

    x2u(-3 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100000") == 0);

    x2u(-3.125 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100011") == 0);

    x2u(-3.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100010") == 0);

    x2u(-3.375 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101100011") == 0);

    x2u(-3.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100001") == 0);

    x2u(-3.625 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100011") == 0);

    x2u(-3.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100010") == 0);

    x2u(-3.875 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111100011") == 0);

    x2u(-4 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000100") == 0);

    x2u(-4.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000100111") == 0);

    x2u(-4.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100110") == 0);

    x2u(-4.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001100111") == 0);

    x2u(-5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100101") == 0);

    x2u(-5.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010100111") == 0);

    x2u(-5.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101100110") == 0);

    x2u(-5.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011100111") == 0);

    x2u(-6 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100100") == 0);

    x2u(-6.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100100111") == 0);

    x2u(-6.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100110") == 0);

    x2u(-6.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101100111") == 0);

    x2u(-7 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100101") == 0);

    x2u(-7.25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110100111") == 0);

    x2u(-7.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111100110") == 0);

    x2u(-7.75 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111111100111") == 0);

    x2u(-8 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001000") == 0);

    x2u(-8.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000101011") == 0);

    x2u(-9 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000101010") == 0);

    x2u(-9.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001101011") == 0);

    x2u(-10 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101001") == 0);

    x2u(-10.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010101011") == 0);

    x2u(-11 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001101010") == 0);

    x2u(-11.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011101011") == 0);

    x2u(-12 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101000") == 0);

    x2u(-12.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100101011") == 0);

    x2u(-13 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010101010") == 0);

    x2u(-13.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101101011") == 0);

    x2u(-14 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101001") == 0);

    x2u(-14.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110101011") == 0);

    x2u(-15 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011101010") == 0);

    x2u(-15.5 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111101011") == 0);

    x2u(-16 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001000") == 0);

    x2u(-17 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111000101011") == 0);

    x2u(-18 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100101010") == 0);

    x2u(-19 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001101011") == 0);

    x2u(-20 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101001") == 0);

    x2u(-21 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010101011") == 0);

    x2u(-22 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101101010") == 0);

    x2u(-23 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011101011") == 0);

    x2u(-24 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101000") == 0);

    x2u(-25 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100101011") == 0);

    x2u(-26 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110101010") == 0);

    x2u(-27 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101101011") == 0);

    x2u(-28 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101001") == 0);

    x2u(-29 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110101011") == 0);

    x2u(-30 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111111101010") == 0);

    x2u(-31 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111111101011") == 0);

    x2u(-32 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001100") == 0);

    x2u(-34 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100000101111") == 0);

    x2u(-36 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000101110") == 0);

    x2u(-38 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001101111") == 0);

    x2u(-40 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000101101") == 0);

    x2u(-42 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010101111") == 0);

    x2u(-44 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001101110") == 0);

    x2u(-46 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100011101111") == 0);

    x2u(-48 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101100") == 0);

    x2u(-50 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100101111") == 0);

    x2u(-52 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010101110") == 0);

    x2u(-54 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101101111") == 0);

    x2u(-56 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001101101") == 0);

    x2u(-58 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110101111") == 0);

    x2u(-60 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011101110") == 0);

    x2u(-62 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111101111") == 0);

    x2u(-64 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001100") == 0);

    x2u(-68 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101000101111") == 0);

    x2u(-72 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100101110") == 0);

    x2u(-76 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001101111") == 0);

    x2u(-80 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010101101") == 0);

    x2u(-84 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010101111") == 0);

    x2u(-88 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101101110") == 0);

    x2u(-92 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011101111") == 0);

    x2u(-96 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101100") == 0);

    x2u(-100 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101100101111") == 0);

    x2u(-104 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110101110") == 0);

    x2u(-108 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101101111") == 0);

    x2u(-112 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011101101") == 0);

    x2u(-116 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110101111") == 0);

    x2u(-120 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111101110") == 0);

    x2u(-124 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111101111") == 0);

    x2u(-128 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001100") == 0);

    x2u(-136 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110000101111") == 0);

    x2u(-144 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111000101110") == 0);

    x2u(-152 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001101111") == 0);

    x2u(-160 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100101101") == 0);

    x2u(-168 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010101111") == 0);

    x2u(-176 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001101110") == 0);

    x2u(-184 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110011101111") == 0);

    x2u(-192 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101100") == 0);

    x2u(-200 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100101111") == 0);

    x2u(-208 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010101110") == 0);

    x2u(-216 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101101111") == 0);

    x2u(-224 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101101101") == 0);

    x2u(-232 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110101111") == 0);

    x2u(-240 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011101110") == 0);

    x2u(-248 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111101111") == 0);

    x2u(-256 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001100") == 0);

    x2u(-272 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111000101111") == 0);

    x2u(-288 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100101110") == 0);

    x2u(-304 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001101111") == 0);

    x2u(-320 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110101101") == 0);

    x2u(-336 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010101111") == 0);

    x2u(-352 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101101110") == 0);

    x2u(-368 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011101111") == 0);

    x2u(-384 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101100") == 0);

    x2u(-400 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111100101111") == 0);

    x2u(-416 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110101110") == 0);

    x2u(-432 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101101111") == 0);

    x2u(-448 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111111101101") == 0);

    x2u(-464 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111110101111") == 0);

    x2u(-480 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111111101110") == 0);

    x2u(-INFINITY ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111111101111") == 0);

    x2u(0.000705135 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000000011111") == 0);

    x2u(0.001088567 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000000111111") == 0);

    x2u(0.002070355 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000001011111") == 0);

    x2u(0.003529284 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000001111111") == 0);

    x2u(0.004296151 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000010011111") == 0);

    x2u(0.004917854 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000010111111") == 0);

    x2u(0.00681836 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000011011111") == 0);

    x2u(0.0072369 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000011111111") == 0);

    x2u(0.007989199 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000100011111") == 0);

    x2u(0.009591506 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000100111111") == 0);

    x2u(0.009883381 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101011111") == 0);

    x2u(0.011014734 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000101111111") == 0);

    x2u(0.012182761 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000110011111") == 0);

    x2u(0.013103449 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000110111111") == 0);

    x2u(0.014543647 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111011111") == 0);

    x2u(0.014947136 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00000111111111") == 0);

    x2u(0.015743775 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001000011111") == 0);

    x2u(0.016650196 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001000111111") == 0);

    x2u(0.01813216 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001011111") == 0);

    x2u(0.01936854 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001001111111") == 0);

    x2u(0.019891206 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001010011111") == 0);

    x2u(0.021144409 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001010111111") == 0);

    x2u(0.02163236 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011011111") == 0);

    x2u(0.022876324 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001011111111") == 0);

    x2u(0.024385955 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001100011111") == 0);

    x2u(0.024969826 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001100111111") == 0);

    x2u(0.025922176 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101011111") == 0);

    x2u(0.027240862 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001101111111") == 0);

    x2u(0.027792907 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001110011111") == 0);

    x2u(0.028451417 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001110111111") == 0);

    x2u(0.029446582 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111011111") == 0);

    x2u(0.030884182 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00001111111111") == 0);

    x2u(0.031675476 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010000011111") == 0);

    x2u(0.034940529 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010000111111") == 0);

    x2u(0.036657694 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001011111") == 0);

    x2u(0.037429178 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010001111111") == 0);

    x2u(0.03928521 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010010011111") == 0);

    x2u(0.042705765 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010010111111") == 0);

    x2u(0.044724116 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010011011111") == 0);

    x2u(0.046777591 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010011111111") == 0);

    x2u(0.048405731 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010100011111") == 0);

    x2u(0.050515144 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010100111111") == 0);


    x2u(0.052482664 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101011111") == 0);

    x2u(0.053344962 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010101111111") == 0);

    x2u(0.05612053 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010110011111") == 0);

    x2u(0.057537554 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010110111111") == 0);

    x2u(0.05932369 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111011111") == 0);

    x2u(0.060631403 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00010111111111") == 0);

    x2u(0.065971146 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011000011111") == 0);

    x2u(0.069050539 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011000111111") == 0);

    x2u(0.072303444 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001011111") == 0);

    x2u(0.074258986 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011001111111") == 0);

    x2u(0.080851555 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011010011111") == 0);

    x2u(0.08254777 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011010111111") == 0);

    x2u(0.086416379 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011011111") == 0);

    x2u(0.091571553 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011011111111") == 0);

    x2u(0.095404546 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011100011111") == 0);

    x2u(0.098925304 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011100111111") == 0);

    x2u(0.101898475 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101011111") == 0);

    x2u(0.106642955 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011101111111") == 0);

    x2u(0.113232311 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011110011111") == 0);

    x2u(0.116902779 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011110111111") == 0);

    x2u(0.120676081 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111011111") == 0);

    x2u(0.121101156 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00011111111111") == 0);

    x2u(0.129197849 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100000011111") == 0);

    x2u(0.140093765 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100000111111") == 0);

    x2u(0.145688746 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001011111") == 0);

    x2u(0.155768453 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100001111111") == 0);

    x2u(0.162183765 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100010011111") == 0);

    x2u(0.170788061 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100010111111") == 0);

    x2u(0.172499563 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100011011111") == 0);

    x2u(0.180403309 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100011111111") == 0);


    x2u(0.190348245 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100100011111") == 0);

    x2u(0.200606929 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100100111111") == 0);

    x2u(0.204367958 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101011111") == 0);

    x2u(0.218610206 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100101111111") == 0);

    x2u(0.224635425 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100110011111") == 0);

    x2u(0.234045148 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100110111111") == 0);

    x2u(0.235012529 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111011111") == 0);

    x2u(0.243223502 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "00100111111111") == 0);

    x2u(0.252009155 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001000011011") == 0);

    x2u(0.266976747 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001000111011") == 0);

    x2u(0.285408824 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001011011") == 0);

    x2u(0.310682807 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001001111011") == 0);

    x2u(0.32531033 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010011011") == 0);

    x2u(0.330822012 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001010111011") == 0);

    x2u(0.351011924 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001011011011") == 0);

    x2u(0.363514203 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001011111011") == 0);

    x2u(0.375916418 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001100011011") == 0);

    x2u(0.397393951 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001100111011") == 0);

    x2u(0.407596806 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101011011") == 0);

    x2u(0.42621817 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001101111011") == 0);

    x2u(0.451289983 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110011011") == 0);

    x2u(0.463540098 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001110111011") == 0);

    x2u(0.47510355 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001111011011") == 0);

    x2u(0.49967394 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0001111111011") == 0);

    x2u(0.507738305 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000011011") == 0);

    x2u(0.557122775 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010000111011") == 0);

    x2u(0.566396509 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001011011") == 0);

    x2u(0.59378366 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010001111011") == 0);

    x2u(0.644763383 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010011011") == 0);

    x2u(0.669209389 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010010111011") == 0);

    x2u(0.705987781 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010011011011") == 0);

    x2u(0.737372246 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010011111011") == 0);

    x2u(0.753538782 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010100011011") == 0);

    x2u(0.796482043 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010100111011") == 0);

    x2u(0.826977476 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010101011011") == 0);

    x2u(0.843949193 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010101111011") == 0);

    x2u(0.898133725 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010110011011") == 0);

    x2u(0.908169357 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010110111011") == 0);

    x2u(0.952727511 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010111011011") == 0);

    x2u(0.995046762 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0010111111011") == 0);

    x2u(1.018032946 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000010111") == 0);

    x2u(1.090759581 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001000110111") == 0);

    x2u(1.154366218 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001010111") == 0);

    x2u(1.212132022 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001001110111") == 0);

    x2u(1.252412852 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010010111") == 0);

    x2u(1.325846649 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001010110111") == 0);

    x2u(1.405234099 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011010111") == 0);

    x2u(1.469637608 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001011110111") == 0);

    x2u(1.561598685 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001100010111") == 0);

    x2u(1.621461999 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001100110111") == 0);

    x2u(1.671950935 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101010111") == 0);

    x2u(1.709867406 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001101110111") == 0);

    x2u(1.777248343 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001110010111") == 0);

    x2u(1.853109694 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001110110111") == 0);

    x2u(1.907577963 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111010111") == 0);

    x2u(1.998261501 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "001111110111") == 0);

    x2u(2.084294002 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000010011") == 0);

    x2u(2.15961578 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01000110011") == 0);

    x2u(2.351628711 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01001010011") == 0);

    x2u(2.424588479 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01001110011") == 0);

    x2u(2.559768163 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010010011") == 0);

    x2u(2.631223683 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01010110011") == 0);

    x2u(2.796375517 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011010011") == 0);

    x2u(2.965480095 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011110011") == 0);

    x2u(3.111795545 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010011") == 0);

    x2u(3.230585257 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110011") == 0);

    x2u(3.370526512 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010011") == 0);

    x2u(3.447847479 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110011") == 0);

    x2u(3.519548092 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010011") == 0);

    x2u(3.655848959 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110011") == 0);

    x2u(3.798590639 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010011") == 0);

    x2u(3.905186226 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "010111110111") == 0);

    x2u(4.011810571 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000010111") == 0);

    x2u(4.497797874 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011000110111") == 0);

    x2u(4.700182491 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001010111") == 0);

    x2u(4.80627864 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011001110111") == 0);

    x2u(5.106007645 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010010111") == 0);

    x2u(5.271620349 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011010110111") == 0);

    x2u(5.546212477 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011010111") == 0);

    x2u(5.935364227 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011011110111") == 0);

    x2u(6.175271081 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100010111") == 0);

    x2u(6.357345002 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011100110111") == 0);

    x2u(6.709311352 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101010111") == 0);

    x2u(6.763388755 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011101110111") == 0);

    x2u(7.002377703 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110010111") == 0);

    x2u(7.45515116 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011110110111") == 0);

    x2u(7.540803293 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "011111010111") == 0);

    x2u(7.969314455 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0101111111011") == 0);

    x2u(8.26423787 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000011011") == 0);

    x2u(8.620491446 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110000111011") == 0);

    x2u(9.256248212 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001011011") == 0);

    x2u(9.906547269 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110001111011") == 0);

    x2u(10.23811265 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010011011") == 0);

    x2u(10.75546361 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110010111011") == 0);

    x2u(11.05082605 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011011011") == 0);

    x2u(11.74254584 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110011111011") == 0);

    x2u(12.02202487 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100011011") == 0);

    x2u(12.97637635 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110100111011") == 0);

    x2u(13.32004951 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101011011") == 0);

    x2u(13.95254669 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110101111011") == 0);

    x2u(14.4508715 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110011011") == 0);

    x2u(14.8679784 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110110111011") == 0);

    x2u(15.38461342 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111011011") == 0);

    x2u(15.50139547 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0110111111011") == 0);

    x2u(16.27106774 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111000011011") == 0);

    x2u(17.04012841 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111000111011") == 0);

    x2u(18.36811185 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001011011") == 0);

    x2u(19.47911834 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111001111011") == 0);

    x2u(20.16346006 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010011011") == 0);

    x2u(21.1011499 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111010111011") == 0);

    x2u(22.33641326 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011011011") == 0);

    x2u(23.28126542 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111011111011") == 0);

    x2u(24.5571561 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100011011") == 0);

    x2u(25.85430372 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111100111011") == 0);

    x2u(26.51973596 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101011011") == 0);

    x2u(27.5284979 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111101111011") == 0);

    x2u(28.05236523 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110011011") == 0);

    x2u(29.2055704 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111110111011") == 0);

    x2u(30.59248044 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "0111111011011") == 0);

    x2u(31.17318749 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01011111111111") == 0);

    x2u(33.59497764 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100000011111") == 0);

    x2u(35.23872219 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100000111111") == 0);

    x2u(37.67920063 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001011111") == 0);

    x2u(39.90821247 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100001111111") == 0);

    x2u(41.68240076 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010011111") == 0);

    x2u(43.6717296 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100010111111") == 0);

    x2u(44.95598089 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100011011111") == 0);

    x2u(46.72600349 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100011111111") == 0);

    x2u(49.60083491 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100011111") == 0);

    x2u(51.4752132 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100100111111") == 0);

    x2u(53.3267202 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101011111") == 0);

    x2u(55.73221894 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100101111111") == 0);

    x2u(56.21694478 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110011111") == 0);

    x2u(58.74538983 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100110111111") == 0);

    x2u(61.93548748 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111011111") == 0);

    x2u(62.97286531 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01100111111111") == 0);

    x2u(64.29425567 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101000011111") == 0);

    x2u(70.710033 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101000111111") == 0);

    x2u(72.46197393 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001011111") == 0);

    x2u(78.29308725 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101001111111") == 0);

    x2u(83.27160067 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010011111") == 0);

    x2u(86.81265256 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101010111111") == 0);

    x2u(88.53183035 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011011111") == 0);

    x2u(93.11976742 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101011111111") == 0);

    x2u(98.95771956 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101100011111") == 0);

    x2u(100.9335558 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101100111111") == 0);

    x2u(104.2996767 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101011111") == 0);

    x2u(111.1578942 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101101111111") == 0);

    x2u(115.5780035 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110011111") == 0);

    x2u(117.83341 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101110111111") == 0);

    x2u(122.2127794 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111011111") == 0);

    x2u(126.0834115 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01101111111111") == 0);

    x2u(135.8250541 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110000011111") == 0);

    x2u(136.7418256 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110000111111") == 0);

    x2u(150.3935415 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001011111") == 0);

    x2u(158.7182407 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110001111111") == 0);

    x2u(167.3340627 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010011111") == 0);

    x2u(169.1647124 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110010111111") == 0);

    x2u(179.8271804 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110011011111") == 0);

    x2u(186.0065266 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110011111111") == 0);

    x2u(193.6405534 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100011111") == 0);

    x2u(202.2005897 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110100111111") == 0);

    x2u(208.1572907 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101011111") == 0);

    x2u(216.5601156 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110101111111") == 0);

    x2u(231.4100622 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110011111") == 0);

    x2u(233.9554994 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110110111111") == 0);

    x2u(240.9349852 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111011111") == 0);

    x2u(253.0211966 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01110111111111") == 0);

    x2u(259.2161655 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111000011111") == 0);

    x2u(275.5652048 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111000111111") == 0);

    x2u(296.0763936 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001011111") == 0);

    x2u(312.0327506 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111001111111") == 0);

    x2u(331.5448058 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010011111") == 0);

    x2u(343.9592489 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111010111111") == 0);

    x2u(361.6828094 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011011111") == 0);

    x2u(370.2378765 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111011111111") == 0);

    x2u(385.2384648 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111100011111") == 0);

    x2u(414.8330378 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111100111111") == 0);

    x2u(427.0510401 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101011111") == 0);

    x2u(432.6105446 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111101111111") == 0);

    x2u(456.7125906 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111110011111") == 0);

    x2u(467.5684248 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111110111111") == 0);

    x2u(1111.262324 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111011111") == 0);

    x2u(NAN ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "01111111111111") == 0);

    x2u(-0.000335918 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000000011111") == 0);

    x2u(-0.001911957 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000000111111") == 0);

    x2u(-0.002175712 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000001011111") == 0);

    x2u(-0.00378061 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000001111111") == 0);

    x2u(-0.004025373 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000010011111") == 0);

    x2u(-0.005643448 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000010111111") == 0);

    x2u(-0.006269122 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000011011111") == 0);

    x2u(-0.007225197 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000011111111") == 0);

    x2u(-0.008786191 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000100011111") == 0);

    x2u(-0.009580768 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000100111111") == 0);

    x2u(-0.010502551 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101011111") == 0);

    x2u(-0.011224217 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000101111111") == 0);

    x2u(-0.011901588 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000110011111") == 0);

    x2u(-0.013130747 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000110111111") == 0);

    x2u(-0.014239439 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111011111") == 0);

    x2u(-0.015013367 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10000111111111") == 0);

    x2u(-0.016061797 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001000011111") == 0);

    x2u(-0.016932704 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001000111111") == 0);

    x2u(-0.018469316 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001011111") == 0);

    x2u(-0.018567838 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001001111111") == 0);

    x2u(-0.020091415 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001010011111") == 0);

    x2u(-0.021334683 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001010111111") == 0);

    x2u(-0.021647706 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011011111") == 0);

    x2u(-0.022789298 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001011111111") == 0);

    x2u(-0.024330712 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001100011111") == 0);

    x2u(-0.025122374 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001100111111") == 0);

    x2u(-0.025458703 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101011111") == 0);

    x2u(-0.026737557 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001101111111") == 0);

    x2u(-0.027956204 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001110011111") == 0);

    x2u(-0.028997219 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001110111111") == 0);

    x2u(-0.029994134 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111011111") == 0);

    x2u(-0.030476112 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10001111111111") == 0);

    x2u(-0.03183693 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010000011111") == 0);

    x2u(-0.034611095 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010000111111") == 0);

    x2u(-0.036619182 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001011111") == 0);

    x2u(-0.037433082 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010001111111") == 0);

    x2u(-0.040392605 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010010011111") == 0);

    x2u(-0.042916721 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010010111111") == 0);

    x2u(-0.043322432 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010011011111") == 0);

    x2u(-0.045528663 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010011111111") == 0);

    x2u(-0.048766735 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010100011111") == 0);

    x2u(-0.049823756 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010100111111") == 0);

    x2u(-0.05088326 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101011111") == 0);

    x2u(-0.054089089 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010101111111") == 0);

    x2u(-0.056073337 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010110011111") == 0);

    x2u(-0.058027895 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010110111111") == 0);

    x2u(-0.058905674 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111011111") == 0);

    x2u(-0.060757904 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10010111111111") == 0);

    x2u(-0.06549986 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011000011111") == 0);

    x2u(-0.06661182 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011000111111") == 0);

    x2u(-0.07406502 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001011111") == 0);

    x2u(-0.075850092 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011001111111") == 0);

    x2u(-0.079294504 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011010011111") == 0);

    x2u(-0.08529298 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011010111111") == 0);

    x2u(-0.087546521 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011011111") == 0);

    x2u(-0.09074077 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011011111111") == 0);

    x2u(-0.096449611 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011100011111") == 0);

    x2u(-0.099502671 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011100111111") == 0);

    x2u(-0.10458845 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101011111") == 0);

    x2u(-0.108150517 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011101111111") == 0);

    x2u(-0.112108967 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011110011111") == 0);

    x2u(-0.117134805 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011110111111") == 0);

    x2u(-0.120739722 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111011111") == 0);

    x2u(-0.12355327 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10011111111111") == 0);

    x2u(-0.131219675 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100000011111") == 0);

    x2u(-0.134339103 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100000111111") == 0);

    x2u(-0.147956782 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001011111") == 0);

    x2u(-0.154721043 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100001111111") == 0);

    x2u(-0.157371669 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100010011111") == 0);

    x2u(-0.168302752 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100010111111") == 0);

    x2u(-0.175970736 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100011011111") == 0);

    x2u(-0.185916552 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100011111111") == 0);

    x2u(-0.189613632 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100100011111") == 0);

    x2u(-0.198601022 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100100111111") == 0);

    x2u(-0.205385835 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101011111") == 0);

    x2u(-0.215661185 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100101111111") == 0);

    x2u(-0.226044395 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100110011111") == 0);

    x2u(-0.232710746 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100110111111") == 0);

    x2u(-0.237820074 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111011111") == 0);

    x2u(-0.244444263 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "10100111111111") == 0);

    x2u(-0.265112162 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001000011011") == 0);

    x2u(-0.270664221 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001000111011") == 0);

    x2u(-0.290121511 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001011011") == 0);

    x2u(-0.307631899 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001001111011") == 0);

    x2u(-0.322318623 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010011011") == 0);

    x2u(-0.342110213 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001010111011") == 0);

    x2u(-0.355111391 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001011011011") == 0);

    x2u(-0.368493616 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001011111011") == 0);

    x2u(-0.375142621 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001100011011") == 0);

    x2u(-0.400929362 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001100111011") == 0);

    x2u(-0.406692737 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101011011") == 0);

    x2u(-0.434279053 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001101111011") == 0);

    x2u(-0.450921714 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110011011") == 0);

    x2u(-0.465045882 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001110111011") == 0);

    x2u(-0.475965677 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001111011011") == 0);

    x2u(-0.494604013 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1001111111011") == 0);

    x2u(-0.502696323 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000011011") == 0);

    x2u(-0.540546949 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010000111011") == 0);

    x2u(-0.59230092 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001011011") == 0);

    x2u(-0.605992361 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010001111011") == 0);

    x2u(-0.630806704 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010011011") == 0);

    x2u(-0.675242358 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010010111011") == 0);

    x2u(-0.714323797 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010011011011") == 0);

    x2u(-0.74669071 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010011111011") == 0);

    x2u(-0.772340496 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010100011011") == 0);

    x2u(-0.788415892 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010100111011") == 0);

    x2u(-0.828394172 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010101011011") == 0);

    x2u(-0.858141491 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010101111011") == 0);

    x2u(-0.888091035 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010110011011") == 0);

    x2u(-0.93207296 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010110111011") == 0);

    x2u(-0.961798348 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010111011011") == 0);

    x2u(-0.998064419 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1010111111011") == 0);

    x2u(-1.021640563 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000010111") == 0);

    x2u(-1.09184627 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101000110111") == 0);

    x2u(-1.132901408 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001010111") == 0);

    x2u(-1.230094842 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101001110111") == 0);

    x2u(-1.297531649 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010010111") == 0);

    x2u(-1.346206262 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101010110111") == 0);

    x2u(-1.431689253 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011010111") == 0);

    x2u(-1.471647918 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101011110111") == 0);

    x2u(-1.502379323 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101100010111") == 0);

    x2u(-1.59265255 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101100110111") == 0);

    x2u(-1.636057684 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101010111") == 0);

    x2u(-1.711835722 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101101110111") == 0);

    x2u(-1.799619938 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101110010111") == 0);

    x2u(-1.861654421 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101110110111") == 0);

    x2u(-1.919029168 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111010111") == 0);

    x2u(-1.942071974 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "101111110111") == 0);

    x2u(-2.063201745 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000010011") == 0);

    x2u(-2.185572281 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11000110011") == 0);

    x2u(-2.282349521 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11001010011") == 0);

    x2u(-2.468648431 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11001110011") == 0);

    x2u(-2.583275014 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010010011") == 0);

    x2u(-2.682537779 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11010110011") == 0);

    x2u(-2.848372729 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011010011") == 0);

    x2u(-2.957998019 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011110011") == 0);

    x2u(-3.014196071 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010011") == 0);

    x2u(-3.157463979 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110011") == 0);

    x2u(-3.279964554 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010011") == 0);

    x2u(-3.442421512 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110011") == 0);

    x2u(-3.504686357 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010011") == 0);

    x2u(-3.729299893 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110011") == 0);

    x2u(-3.798550368 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010011") == 0);

    x2u(-3.945628608 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "110111110111") == 0);

    x2u(-4.126767658 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000010111") == 0);

    x2u(-4.484788109 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111000110111") == 0);

    x2u(-4.614567495 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001010111") == 0);

    x2u(-4.819003439 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111001110111") == 0);

    x2u(-5.012312449 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010010111") == 0);

    x2u(-5.332556574 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111010110111") == 0);

    x2u(-5.59806907 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011010111") == 0);

    x2u(-5.799892984 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111011110111") == 0);

    x2u(-6.146659975 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100010111") == 0);

    x2u(-6.252608988 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111100110111") == 0);

    x2u(-6.558556115 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101010111") == 0);

    x2u(-6.997704261 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111101110111") == 0);

    x2u(-7.01089902 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110010111") == 0);

    x2u(-7.414088 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111110110111") == 0);

    x2u(-7.635332168 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "111111010111") == 0);

    x2u(-7.875414746 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1101111111011") == 0);

    x2u(-8.339474314 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000011011") == 0);

    x2u(-8.761308293 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110000111011") == 0);

    x2u(-9.426939952 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001011011") == 0);

    x2u(-9.653057267 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110001111011") == 0);

    x2u(-10.42898883 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010011011") == 0);

    x2u(-10.58437418 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110010111011") == 0);

    x2u(-11.20689804 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011011011") == 0);

    x2u(-11.78283645 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110011111011") == 0);

    x2u(-12.4001054 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100011011") == 0);

    x2u(-12.7633776 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110100111011") == 0);

    x2u(-13.37509948 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101011011") == 0);

    x2u(-13.58931863 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110101111011") == 0);

    x2u(-14.18771293 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110011011") == 0);

    x2u(-14.65089909 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110110111011") == 0);

    x2u(-15.29659802 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111011011") == 0);

    x2u(-15.9859961 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1110111111011") == 0);

    x2u(-16.52911755 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111000011011") == 0);

    x2u(-17.67468052 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111000111011") == 0);

    x2u(-18.82968716 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001011011") == 0);

    x2u(-19.50095716 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111001111011") == 0);

    x2u(-20.84511668 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010011011") == 0);

    x2u(-21.79586756 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111010111011") == 0);

    x2u(-22.6658901 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011011011") == 0);

    x2u(-23.09263284 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111011111011") == 0);

    x2u(-24.2028784 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100011011") == 0);

    x2u(-25.00642838 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111100111011") == 0);

    x2u(-26.79455275 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101011011") == 0);

    x2u(-27.41604066 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111101111011") == 0);

    x2u(-28.51383594 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110011011") == 0);

    x2u(-29.3761716 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111110111011") == 0);

    x2u(-30.15518153 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "1111111011011") == 0);

    x2u(-31.45031186 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11011111111111") == 0);

    x2u(-33.11918653 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100000011111") == 0);

    x2u(-34.88334257 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100000111111") == 0);

    x2u(-36.45308054 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001011111") == 0);

    x2u(-38.19823622 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100001111111") == 0);

    x2u(-40.73851561 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010011111") == 0);

    x2u(-43.21642302 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100010111111") == 0);

    x2u(-44.85283718 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100011011111") == 0);

    x2u(-47.76925141 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100011111111") == 0);

    x2u(-48.56327808 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100011111") == 0);

    x2u(-50.9391744 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100100111111") == 0);

    x2u(-52.12726119 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101011111") == 0);

    x2u(-55.0025288 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100101111111") == 0);

    x2u(-56.75149839 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110011111") == 0);

    x2u(-58.7893755 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100110111111") == 0);

    x2u(-60.86932397 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111011111") == 0);

    x2u(-63.14270501 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11100111111111") == 0);

    x2u(-67.51256149 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101000011111") == 0);

    x2u(-71.15704056 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101000111111") == 0);

    x2u(-74.40934831 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001011111") == 0);

    x2u(-78.65687533 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101001111111") == 0);

    x2u(-81.30499448 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010011111") == 0);

    x2u(-84.4999892 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101010111111") == 0);

    x2u(-89.72921511 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011011111") == 0);

    x2u(-92.75388103 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101011111111") == 0);

    x2u(-97.78293834 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101100011111") == 0);

    x2u(-102.7712828 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101100111111") == 0);

    x2u(-106.4154777 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101011111") == 0);

    x2u(-109.5124801 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101101111111") == 0);

    x2u(-114.5458223 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110011111") == 0);

    x2u(-119.993895 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101110111111") == 0);

    x2u(-122.4539414 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111011111") == 0);

    x2u(-124.2060525 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11101111111111") == 0);

    x2u(-131.3101369 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110000011111") == 0);

    x2u(-142.8223156 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110000111111") == 0);

    x2u(-147.3473434 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001011111") == 0);

    x2u(-158.6754958 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110001111111") == 0);

    x2u(-163.7953334 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010011111") == 0);

    x2u(-170.533716 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110010111111") == 0);

    x2u(-176.8674563 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110011011111") == 0);

    x2u(-190.6911384 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110011111111") == 0);

    x2u(-196.8829274 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100011111") == 0);

    x2u(-206.0661254 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110100111111") == 0);

    x2u(-215.4796506 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101011111") == 0);

    x2u(-218.6610471 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110101111111") == 0);

    x2u(-224.9477726 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110011111") == 0);

    x2u(-236.8418225 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110110111111") == 0);

    x2u(-246.4742542 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111011111") == 0);

    x2u(-252.8052908 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11110111111111") == 0);

    x2u(-261.0148479 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111000011111") == 0);

    x2u(-275.3711185 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111000111111") == 0);

    x2u(-302.965284 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001011111") == 0);

    x2u(-306.3295767 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111001111111") == 0);

    x2u(-329.14432 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010011111") == 0);

    x2u(-339.8455304 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111010111111") == 0);

    x2u(-359.4660976 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011011111") == 0);

    x2u(-368.6852077 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111011111111") == 0);

    x2u(-391.417856 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111100011111") == 0);

    x2u(-407.5620728 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111100111111") == 0);

    x2u(-424.1474937 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101011111") == 0);

    x2u(-432.6610609 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111101111111") == 0);

    x2u(-450.4261045 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111110011111") == 0);

    x2u(-466.7579468 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111110111111") == 0);

    x2u(-673.879947 ,&u);
    get_unum_string_for_test(actual, u);
    assert(strcmp(actual, "11111111011111") == 0);

	printf("All x2u tests for {2,2} passed.\n");
	return;
}
