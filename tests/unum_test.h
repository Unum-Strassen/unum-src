/*
 * Header file for unum functions
 */
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <libunum.h>
#include <time.h>
#include <gmp.h>
#include <stdlib.h>

#ifndef UNUM_TEST_H
#define UNUM_TEST_H

/*
 * Test x2u for all environments
 */
void test_x2u_all_envs();

/*
 * Test x2u for environment {0,0}
 */
void test_x2u_0_0();

/*
 * Test x2u for environment {0,1}
 */
void test_x2u_0_1();

/*
 * Test x2u for environment {1,0}
 */
void test_x2u_1_0();

/*
 * Test x2u for environment {1,1}
 */
void test_x2u_1_1();

/*
 * Test x2u for environment {1,2}
 */
void test_x2u_1_2();

/*
 * Test x2u for environment {2,1}
 */
void test_x2u_2_1();

/*
 * Test x2u for environment {2,2}
 */
void test_x2u_2_2();

/*
 * Test x2u for environment {3,6}
 */
void test_x2u_3_6();

/*
 * Returns the bit string of the unum
 */
void get_unum_string_for_test(char* buffer, unum_t u);

/*
 * Returns the bit string of the specified field
 */
void get_bit_string_for_test(char* buffer, unsigned long long x, int width);

/*
 * Returns the bit string of the ubound
 */
void get_ubound_string_for_test(char* ubound_string, ubound_t x);

/*
 * Test the unum multiplication for all environments
 */
void test_timesu_all_envs();

/*
 * Test the unum multiplication for environment {0,0}
 */
void test_timesu_0_0();

/*
 * Test the unum multiplication for environment {0,1}
 */
void test_timesu_0_1();

/*
 * Test the unum multiplication for environment {1,0}
 */
void test_timesu_1_0();

/*
 * Test the unum multiplication for environment {1,1}
 */
void test_timesu_1_1();

/*
 * Test the unum multiplication for environment {4,4}
 * with random values
 */
void test_timesu_4_4();

/*
 * Test the unum multiplication for environment {3,5}
 * with random values
 */
void test_timesu_3_5();

/*
 * Test speed performance for multiplication
 */
void test_mul_perf();

/*
 * Test the unum division for environment {0,0}
 */
void test_divideu_0_0();

/*
 * Test the unum division for environment {0,1}
 */
void test_divideu_0_1();

/*
 * Test the unum division for environment {1,0}
 */
void test_divideu_1_0();

/*
 * Test the unum division for environment {1,1}
 */
void test_divideu_1_1();

/*
 * Test the unum division for environment {4,4}
 */
void test_divideu_4_4();

/*
 * Test the unum division for environment {3,5}
 * with random values
 */
void test_divideu_3_5();

/*
 * Test the unum division for all environments
 */
void test_divideu_all_envs();

/*
 * Test the unum square root for environment {0,0}
 */
void test_sqrtu_0_0();

/*
 * Test the unum square root for environment {0,1}
 */
void test_sqrtu_0_1();

/*
 * Test the unum square root for environment {1,0}
 */
void test_sqrtu_1_0();

/*
 * Test the unum square root for environment {1,1}
 */
void test_sqrtu_1_1();

/*
 * Test the unum square root for environment {2,2}
 */
void test_sqrtu_2_2();

/*
 * Test the unum square root for all environments
 */
void test_sqrtu_all_envs();

/*
 * Test unum addition for environment {0,0}
 */
void test_plusu_0_0();

/*
 * Test unum addition for environment {0,1}
 */
void test_plusu_0_1();

/*
 * Test unum addition for environment {1,0}
 */
void test_plusu_1_0();

/*
 * Test unum addition for environment {1,1}
 */
void test_plusu_1_1();

/*
 * Test unum addition for environment {4,4}
 */
void test_plusu_4_4();

/*
 * Test unum addition for environment {3,5}
 */
void test_plusu_3_5();

/*
 * Test unum addition for all environments
 */
void test_plusu_all_envs();

/*
 * Test unum subtraction for environment {1,1}
 */
void test_minusu_1_1();

/*
 * Test unum subtraction for all environments
 */
void test_minusu_all_envs();

/*
 * Test unum fused dot product
 */
void test_fused_dot_product();

/*
 * Another test for unum fused dot product
 */
void test_fused_dot_product2();
#endif
