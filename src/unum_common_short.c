#include "libunum.h"

void unum_init(unum_t* x)
{
    x = malloc(sizeof(unum_t));
    return;
}

void ubound_init(ubound_t* ub)
{
    ub = malloc(sizeof(ubound_t));
    return;
}

void ubound_from_unum_init(ubound_t* ub, unum_t u)
{
    (*ub).left_bound = u;
    (*ub).right_bound = u;
    return;
}

void set_env(int e_sizesize, int f_sizesize)
{
    _g_esizesize = e_sizesize;
    _g_fsizesize = f_sizesize;
    _g_esizemax = (1 << e_sizesize);
    _g_fsizemax = (1 << f_sizesize);
    _g_maxexpval = (1 << _g_esizemax) - 1;
    _g_maxfracval = (1ULL << _g_fsizemax) - 1;
    _g_maxfracvalhidden = (1ULL << (_g_fsizemax + 1)) - 1;
    _g_utagsize = 1 + e_sizesize + f_sizesize;
    _g_maxubits = 1 + _g_esizemax + _g_fsizemax + _g_utagsize;
    _g_utagmask = (unum_t) {
        .sign = 0, .exponent = 0, .fraction = 0, .ubit = 1, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1)
    };
    _g_ulpu = (unum_t) { .sign = 0, .exponent = 0, .fraction = 1, .ubit = 0, .e_size = 0, .f_size = 0 };
    _g_smallsubnormalu = (unum_t) {
        .sign = 0, .exponent = 0, .fraction = 1, .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1)
    };
    _g_smallnormalu = (unum_t) {
        .sign = 0, .exponent = 1, .fraction = 0, .ubit = 0, .e_size = (_g_esizemax - 1), .f_size = (_g_fsizemax - 1)
    };
    _g_signbigu = (unum_t) { .sign = 0, .exponent = 0, .fraction = 0, .ubit = 0, .e_size = 0, .f_size = 0 };
    _g_posinfu = (unum_t) { .sign = 0,
                            .exponent = ((1 << _g_esizemax) - 1),
                            .fraction = ((1ULL << _g_fsizemax) - 1),
                            .ubit = 0,
                            .e_size = (_g_esizemax - 1),
                            .f_size = (_g_fsizemax - 1) };
    _g_maxrealu = (unum_t) { .sign = 0,
                             .exponent = ((1 << _g_esizemax) - 1),
                             .fraction = ((1ULL << _g_fsizemax) - 2),
                             .ubit = 0,
                             .e_size = (_g_esizemax - 1),
                             .f_size = (_g_fsizemax - 1) };
    _g_minrealu = (unum_t) { .sign = 1,
                             .exponent = ((1 << _g_esizemax) - 1),
                             .fraction = ((1ULL << _g_fsizemax) - 2),
                             .ubit = 0,
                             .e_size = (_g_esizemax - 1),
                             .f_size = (_g_fsizemax - 1) };
    _g_neginfu = (unum_t) { .sign = 1,
                            .exponent = ((1 << _g_esizemax) - 1),
                            .fraction = ((1ULL << _g_fsizemax) - 1),
                            .ubit = 0,
                            .e_size = (_g_esizemax - 1),
                            .f_size = (_g_fsizemax - 1) };
    _g_negbigu = (unum_t) { .sign = 0,
                            .exponent = ((1 << _g_esizemax) - 1),
                            .fraction = ((1ULL << _g_fsizemax) - 1),
                            .e_size = _g_esizemax - 1,
                            .f_size = _g_fsizemax - 1,
                            .ubit = 1 };
    _g_qNaNu = (unum_t) { .sign = 0,
                          .exponent = ((1 << _g_esizemax) - 1),
                          .fraction = ((1ULL << _g_fsizemax) - 1),
                          .e_size = (_g_esizemax - 1),
                          .f_size = (_g_fsizemax - 1),
                          .ubit = 1 };
    _g_sNaNu = (unum_t) { .sign = 1,
                          .exponent = ((1 << _g_esizemax) - 1),
                          .fraction = ((1ULL << _g_fsizemax) - 1),
                          .e_size = (_g_esizemax - 1),
                          .f_size = (_g_fsizemax - 1),
                          .ubit = 1 };
    if (_g_utagsize == 1)
        _g_negopeninfu = (unum_t) { .sign = 1, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
    else
        _g_negopeninfu = (unum_t) { .sign = 1, .exponent = 1, .fraction = 1, .ubit = 1, .e_size = 0, .f_size = 0 };
    if (_g_utagsize == 1)
        _g_posopeninfu = (unum_t) { .sign = 0, .exponent = 1, .fraction = 0, .ubit = 1, .e_size = 0, .f_size = 0 };
    else
        _g_posopeninfu = (unum_t) { .sign = 0, .exponent = 1, .fraction = 1, .ubit = 1, .e_size = 0, .f_size = 0 };
    _g_zero = (unum_t) { 0, 0, 0, 0, 0, 0 };
    _g_negzero = (unum_t) { 1, 0, 0, 0, 0, 0 };
    _g_maxreal = pow(2, pow(2, (_g_esizemax - 1))) * (pow(2, _g_fsizemax) - 1) / pow(2, (_g_fsizemax - 1));
    _g_smallnormal = pow(2, (1 - ((pow(2, (_g_esizemax - 1)) - 1))));
    _g_smallsubnormal = pow(2, (2 - pow(2, (_g_esizemax - 1)) - _g_fsizemax));
    /*
     * maxexp + bias = 2^esizemax - 1;
     * maxexp = 2^es - 1 - (2^es-1 - 1) = = 2^es - 2^es-1 = 2^es-1
     */
    _g_maxexp = (1 << (_g_esizemax - 1));
    /*
     * For smallsubnormalu to be normalized the fraction has to be multiplied by 2^_g_esizemax
     * Therefore, _g_esizemax needs to be subtracted from the exponent (1-bias)
     */
    _g_minexp = 1 - (1 << (_g_esizemax - 1)) - _g_esizemax;
}

double u2f(unum_t u)
{
    double f, fraction;
    int exponent;

    if (isPosInf(u))
        return INFINITY;

    if (isNegInf(u))
        return -INFINITY;

    if (isNaN(u))
        return NAN;

    if (unum_compare(u, _g_sNaNu))
        return NAN;

    if (u.exponent == 0) {
        exponent = 1 - (pow(2, u.e_size) - 1);
        fraction = u.fraction / pow(2, (u.f_size + 1));
    } else {
        exponent = u.exponent - (pow(2, u.e_size) - 1);
        fraction = 1 + u.fraction / pow(2, (u.f_size + 1));
    }

    f = pow(-1, u.sign) * pow(2, exponent) * fraction;

    return f;
}

/**
 * Returns true if two unums are equal and false otherwise
 */
bool unum_compare(unum_t x, unum_t y)
{
    if (x.sign != y.sign)
        return false;

    if (x.exponent != y.exponent)
        return false;

    if (x.fraction != y.fraction)
        return false;

    if (x.ubit != y.ubit)
        return false;

    if ((x.e_size ^ y.e_size) != 0)
        return false;

    if ((x.f_size ^ y.f_size) != 0)
        return false;
    else
        return true;
}

void x2u(double f, unum_t* u)
{
    double_cast d1;
    unsigned int fraction_bit_length, trailing_zeros;
    unsigned long long intermediate_frac;
    int final_exponent, exp_scale;

    d1.f = f;

    // check for NaN, plus infinity, minus infinity, zero
    if (f != f) {
        *u = _g_qNaNu;
        return;
    } else if (f == INFINITY) {
        *u = _g_posinfu;
        return;
    } else if (f == -INFINITY) {
        *u = _g_neginfu;
        return;
    } else if (f == 0) {
        *u = _g_zero;
        return;
    }

    (*u).sign = d1.parts.sign;
    (*u).ubit = 0;

    if (fabs(f) <= _g_maxreal && fabs(f) >= _g_smallnormal) {
        // normal numbers
        // http://stackoverflow.com/questions/15685181/how-to-get-the-sign-mantissa-and-exponent-of-a-floating-point-number
        if (d1.parts.exponent != 0) {

            // fraction
            if (d1.parts.fraction == 0) {
                if (d1.parts.exponent != DOUBLE_PREC_BIAS) {
                    (*u).f_size = 0;
                    (*u).fraction = 0;
                } else {
                    // unum for 1 is subnormal, therefore handled as a special case
                    (*u).f_size = 0;
                    (*u).fraction = 1;
                    (*u).exponent = 0;
                    (*u).e_size = 0;
                    return;
                }
            } else {

                trailing_zeros = __builtin_ctzll(d1.parts.fraction);
                fraction_bit_length = DOUBLE_PREC_F_SIZE - trailing_zeros;

                if (fraction_bit_length > _g_fsizemax) {
                    // set ubit
                    (*u).ubit = 1;

                    // shift until it fits _g_fsizemax bits
                    d1.parts.fraction = d1.parts.fraction >> (DOUBLE_PREC_F_SIZE - _g_fsizemax);
                    (*u).fraction = d1.parts.fraction;
                    (*u).f_size = _g_fsizemax - 1;
                } else {
                    (*u).f_size = fraction_bit_length - 1;
                    (*u).fraction = d1.parts.fraction >> trailing_zeros;
                }
            }

            // exponent
            final_exponent = d1.parts.exponent - DOUBLE_PREC_BIAS;

        } else {
            // zero - a special case?
            if (d1.parts.fraction == 0) {
                *u = _g_zero;
                return;
            } else {
                trailing_zeros = __builtin_ctzll(d1.parts.fraction);
                fraction_bit_length = DOUBLE_PREC_F_SIZE - trailing_zeros;

                // final exponent = 1 - DOUBLE_PREC_BIAS
                final_exponent = -1022;
                exp_scale = __builtin_clzll(d1.parts.fraction) - (INTERMEDIATE_FRACTION_SIZE - DOUBLE_PREC_BIAS) +
                            1; // the number of leading zeros plus one (for the hidden bit))
                final_exponent = final_exponent - exp_scale;
                fraction_bit_length = fraction_bit_length - exp_scale;

                // set the most significant bit to zero (the hiddent bit)
                d1.parts.fraction &= ~(1ULL << (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(d1.parts.fraction) - 1));

                if (d1.parts.fraction == 0) {
                    (*u).fraction = 0;
                    (*u).f_size = 0;
                    if ((DOUBLE_PREC_F_SIZE - trailing_zeros) > _g_fsizemax) {
                        (*u).ubit = 1;
                        (*u).f_size = _g_fsizemax - 1;
                    }
                } else if (fraction_bit_length > _g_fsizemax) {
                    // set ubit
                    (*u).ubit = 1;
                    // shift until it fits 16 bits
                    (*u).fraction = d1.parts.fraction >> (DOUBLE_PREC_F_SIZE - exp_scale - _g_fsizemax);
                    (*u).f_size = _g_fsizemax - 1;
                } else {
                    (*u).f_size = fraction_bit_length - 1;
                    (*u).fraction = d1.parts.fraction >> trailing_zeros;
                }
            }
        }

        // set exponent and exponent size based on the final exponent value
        if (final_exponent == 1) {
            (*u).e_size = 0;
            (*u).exponent = 1;
        } else {
            (*u).e_size = INTERMEDIATE_EXP_SIZE - __builtin_clz(abs(final_exponent - 1));
            (*u).exponent = final_exponent + ((1 << (*u).e_size) - 1);
        }

        // in case the value is bordering infinity, but shouldn't
        if ((*u).exponent == ((1 << ((*u).e_size + 1)) - 1) && (*u).fraction == ((1ULL << ((*u).f_size + 1)) - 1) &&
            (*u).ubit == 1 && abs(f) < _g_maxreal) {
            // if fraction can be increased, increase that
            if ((*u).f_size < (_g_fsizemax - 1)) {
                (*u).f_size++;
                (*u).fraction = (*u).fraction << 1;
            } else {
                (*u).e_size++;
                (*u).exponent = final_exponent + ((1 << ((*u).e_size)) - 1);
            }
        }

        return;
    }

    // maxrealu
    if (fabsf(f) > _g_maxreal) {
        *u = _g_maxrealu;
        (*u).ubit = 1;
        (*u).sign = d1.parts.sign;
        return;
    }

    // smallsubnormalu
    if (fabsf(f) < _g_smallsubnormal) {
        *u = _g_utagmask;
        (*u).sign = d1.parts.sign;
        return;
    }

    // smallnormalu
    if (fabsf(f) < _g_smallnormal) {
        (*u).exponent = 0;
        (*u).e_size = (_g_esizemax - 1);
        intermediate_frac = d1.parts.fraction;

        if (d1.parts.exponent != 0) {
            // unum exponent - float exponent + 1 (adding one to compensate for hidden bit)
            exp_scale = (1 - ((1 << (*u).e_size) - 1)) - (d1.parts.exponent - DOUBLE_PREC_BIAS + 1);
            intermediate_frac |= (1ULL << DOUBLE_PREC_F_SIZE); // hidden 1
            fraction_bit_length = DOUBLE_PREC_F_SIZE + 1;
        } else {
            exp_scale = (1 - ((1 << (*u).e_size) - 1)) - (1 - DOUBLE_PREC_BIAS);
            fraction_bit_length = DOUBLE_PREC_F_SIZE;
        }

        if (intermediate_frac == 0) {
            (*u).fraction = 0;
            (*u).f_size = 0;
            return;
        }

        // set ubit before clipping
        if (intermediate_frac != 0 && ((fraction_bit_length - __builtin_ctzll(intermediate_frac)) > _g_fsizemax))
            (*u).ubit = 1;

        if (exp_scale < 0) {
            intermediate_frac <<= abs(exp_scale);
            fraction_bit_length -= exp_scale;
        } else {
            // because the fraction size of single precision float(23) is
            // greater than that of the unum type(16), we can do this
            intermediate_frac >>= abs(exp_scale);
        }

        if (fraction_bit_length > _g_fsizemax) {
            (*u).fraction = intermediate_frac >> (fraction_bit_length - _g_fsizemax);
            (*u).f_size = _g_fsizemax - 1;
        } else {
            (*u).fraction = intermediate_frac;
            (*u).f_size = fraction_bit_length - 1;
        }

        // clip off any trailing zeros in the fraction
        if ((*u).fraction != 0 && __builtin_ctzll((*u).fraction) && (*u).ubit == 0) {
            (*u).f_size -= __builtin_ctzll((*u).fraction);
            (*u).fraction >>= __builtin_ctzll((*u).fraction);
        }

        return;
    }
}

void x2ub(double f, ubound_t* ub)
{
    x2u(f, &(*ub).left_bound);
    (*ub).right_bound = (*ub).left_bound;
    return;
}

void print_bits(unsigned long long x, char* colour, unsigned short width)
{
    printf("%s", colour);
    int size, i, leading_zeros = 0;
    unsigned long long bitmask;

    if (width == 0)
        return;

    if (x == 0) {
        for (i = 0; i < width; i++) {
            printf("0");
        }
        printf(RESET);
        return;
    } else {
        size = INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(x) - 1;
        leading_zeros = __builtin_clzll(x) - (INTERMEDIATE_FRACTION_SIZE - width);
        for (i = 0; i < leading_zeros; i++) {
            printf("0");
        }
    }

    for (i = size; i >= 0; i--) {
        bitmask = 1ULL << i;
        // printf("i=%d %llu\n",i,bitmask);
        if (x & bitmask)
            printf("1");
        else
            printf("0");
    }
    printf(RESET);
    return;
}

void printu(unum_t u)
{
    printf("%d\n", u.sign);
    printf("%d\n", u.exponent);
    printf("%llu\n", u.fraction);
    printf("%d\n", u.ubit);
    printf("%d\n", u.e_size);
    printf("%d\n", u.f_size);
}

void unumview(unum_t x)
{
    gbound_t gb;
    printf(KRED "%u " RESET, x.sign);
    print_bits(x.exponent, KBLU, (x.e_size + 1));
    printf(" ");
    print_bits(x.fraction, KBLK, (x.f_size + 1));
    printf(" ");
    printf(KMAG "%d " RESET, x.ubit);
    print_bits(x.e_size, KGRN, _g_esizesize);
    printf(" ");
    print_bits(x.f_size, KYEL, _g_fsizesize);
    printf("\n");

    if (x.sign == 1)
        printf(KRED "- " RESET);
    else
        printf(KRED "+ " RESET);

    if (x.exponent == 0) {
        printf(KBLU "2^%d " RESET, (1 - ((1 << x.e_size) - 1)));
        printf(KBLK "0+(%llu/%llu) " RESET, x.fraction, (1ULL << (x.f_size + 1)));
    } else {
        printf(KBLU "2^%d " RESET, (x.exponent - ((1 << x.e_size) - 1)));
        printf(KBLK "1+(%llu/%llu) " RESET, x.fraction, (1ULL << (x.f_size + 1)));
    }

    if (x.ubit)
        printf(KMAG "~ " RESET);

    printf(KGRN "%d " RESET, x.e_size + 1);
    printf(KYEL "%d " RESET, x.f_size + 1);
    printf("= ");

    if (isNaN(x)) {
        printf("%f", NAN);
    } else if (!x.ubit) {
        printf("%e", u2f(x));
    } else {
        get_gbound_from_unum(&gb, &x);
        if (gb.left_open)
            printf("(");
        else
            printf("[");

        printf("%e, ", u2f(gb.left_bound));
        printf("%e", u2f(gb.right_bound));

        if (gb.right_open)
            printf(")");
        else
            printf("]");
    }

    printf("\n");
}

void uboundview(ubound_t ub)
{
    gbound_t gb;
    unumview(ub.left_bound);
    unumview(ub.right_bound);
    if (ub.left_bound.ubit == 1)
        printf("(");
    else
        printf("[");

    get_gbound_from_ubound(&gb, &ub);
    printf("%e, ", u2f(gb.left_bound));
    printf("%e", u2f(gb.right_bound));

    if (ub.right_bound.ubit == 1)
        printf(")\n\n");
    else
        printf("]\n\n");
}

bool isNaN(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && u.ubit);
}

bool isInf(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
}

bool isNegInf(unum_t u)
{
    return (u.sign && u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
}

bool isPosInf(unum_t u)
{
    return (!u.sign && u.exponent == _g_maxexpval && u.fraction == _g_maxfracval && !u.ubit);
}

bool isNaNOrInf(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == _g_maxfracval);
}

bool isZero(unum_t u)
{
    return !(u.sign | u.exponent | u.fraction | u.ubit);
}

bool isLessThanZero(unum_t u)
{
    if (!u.sign)
        return false;

    if (u.exponent == 0 && u.fraction == 0)
        return false;

    return true;
}

bool isGreaterThanZero(unum_t u)
{
    if (u.sign)
        return false;

    if (u.exponent == 0 && u.fraction == 0)
        return false;

    return true;
}

bool isPosOrNegInexactMaxreal(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == (_g_maxfracval - 1) && u.ubit);
}

bool isPosOrNegMaxreal(unum_t u)
{
    return (u.exponent == _g_maxexpval && u.fraction == (_g_maxfracval - 1));
}

bool isAbsValueEqual(unum_t x, unum_t y)
{
    int x_exp, y_exp, temp_int;
    unsigned long long x_frac = x.fraction, y_frac = y.fraction, temp;

    if (x.e_size == y.e_size && x.exponent == y.exponent && x.f_size == y.f_size && x.fraction == y.fraction)
        return true;

    if (x.exponent) {
        x_exp = x.exponent - ((1 << x.e_size) - 1);
        x_frac = x.fraction | (1ULL << (x.f_size + 1));
    } else
        x_exp = 1 - ((1 << x.e_size) - 1);

    if (y.exponent) {
        y_exp = y.exponent - ((1 << y.e_size) - 1);
        y_frac = y.fraction | (1ULL << (y.f_size + 1));
    } else
        y_exp = 1 - ((1 << y.e_size) - 1);

    temp_int = (x_exp - y_exp + y.f_size - x.f_size);
    if (temp_int > 0) {
        temp = (1ULL << (x_exp - y_exp + y.f_size - x.f_size)) * x_frac;
        if (temp == y_frac)
            return true;
        else
            return false;
    } else {
        temp = (1ULL << (y_exp - x_exp + x.f_size - y.f_size)) * y_frac;
        if (temp == x_frac)
            return true;
        else
            return false;
    }
}

void unify_and_get_result(ubound_t* ub_result, gbound_t gbound)
{

    unum_t left = gbound.left_bound, right = gbound.right_bound, temp, temp2, promoted;
    bool left_open = gbound.left_open, right_open = gbound.right_open;
    int i, j, exponent, scale;
    unsigned long long fraction;

    // if either or both of the bounds are exact and closed,
    // return with the smallest bit representation for the remaining bound
    if (!left_open) {
        ub_result->left_bound = left;

        if (!right_open) {
            ub_result->right_bound = right;
            return;
        } else {
            if (!isNaNOrInf(right) && !right.sign) {
                if (right.fraction)
                    right.fraction--;
                else {
                    right.exponent--;
                    right.fraction = (1ULL << (right.f_size + 1)) - 1;
                }
            } else {
                right = _g_posopeninfu;
            }
            right.ubit = 1;
            ub_result->right_bound = right;
            return;
        }
    }

    if (!right_open) {
        ub_result->right_bound = right;
        if (!isNaNOrInf(left) && left.sign) {
            if (left.fraction)
                left.fraction--;
            else {
                left.exponent--;
                left.fraction = (1ULL << (left.f_size + 1)) - 1;
            }
        } else {
            left = _g_negopeninfu;
        }
        left.ubit = 1;
        ub_result->left_bound = left;
        return;
    }

    // if the left bound is -inf, generate all bit patterns bordering -inf,
    // and check if upper bound is gbound.right_bound
    // Here, if the maximum fraction size is more than 1 bit,
    // there is always a smaller bit representation for inexact maxrealu
    if (isNaNOrInf(left)) {
        for (i = 0; i <= left.f_size; i++) {
            for (j = 0; j <= left.e_size; j++) {
                temp.sign = left.sign;
                temp.ubit = 0;
                temp.f_size = i;
                temp.e_size = j;
                temp.fraction = (1ULL << (temp.f_size + 1)) - 1;
                temp.exponent = (1 << (temp.e_size + 1)) - 1;
                if (isAbsValueEqual(temp, right) && right.sign == left.sign) {
                    temp.ubit = 1;
                    ub_result->left_bound = temp;
                    ub_result->right_bound = temp;
                    return;
                }
            }
        }
    }

    // if the right bound is inf, generate all bit patterns bordering inf,
    // and check if lower bound is gbound.left_bound
    // Here, if the maximum fraction size is more than 1 bit,
    // there is always a smaller bit representation for inexact maxrealu
    if (isNaNOrInf(right)) {
        for (i = 0; i <= right.f_size; i++) {
            for (j = 0; j <= right.e_size; j++) {
                temp.sign = right.sign;
                temp.ubit = 0;
                temp.f_size = i;
                temp.e_size = j;
                temp.fraction = (1ULL << (temp.f_size + 1)) - 1;
                temp.exponent = (1 << (temp.e_size + 1)) - 1;
                if (isAbsValueEqual(temp, left) && right.sign == left.sign) {
                    temp.ubit = 1;
                    ub_result->left_bound = temp;
                    ub_result->right_bound = temp;
                    return;
                }
            }
        }
    }

    // general unification case
    // first get the value with the minimum absolute value
    if (left.sign)
        temp = right;
    else
        temp = left;

    // clip of trailing zeros, if any (this does not change the value of the
    // unum as it is exact)
    if (temp.fraction) {
        i = __builtin_ctzll(temp.fraction);
        temp.fraction >>= i;
        temp.f_size -= i;
    } else {
        temp.fraction = 0;
        temp.f_size = 0;
        if (!temp.exponent)
            temp.e_size = 0;
    }

    // get exponent and fraction of temp
    if (temp.exponent) {
        exponent = temp.exponent - ((1 << temp.e_size) - 1);
        fraction = temp.fraction | (1ULL << (temp.f_size + 1));
    } else {
        exponent = 2 - (1 << temp.e_size); // 1 - bias
        fraction = temp.fraction;
        // normalize
        //(f_size + 1) - (bit length of fraction) + 1
        scale = temp.f_size + 1 - (INTERMEDIATE_FRACTION_SIZE - __builtin_clzll(fraction)) + 1;
        fraction <<= scale;
        exponent -= scale;
    }

    // for each f_size and e_size generate the equivalent unum
    for (i = temp.f_size; i < _g_fsizemax; i++) {
        for (j = temp.e_size; j < _g_esizemax; j++) {
            promoted = temp;
            // generate unum in for i, j f_size and e_size
            if (j != temp.e_size) {
                if (isPosOrNegZero(temp)) {
                    promoted.e_size = j;
                    promoted.f_size = i;
                } else {
                    if (exponent < (2 - (1 << j))) {
                        promoted.exponent = 0;
                        promoted.e_size = j;
                        promoted.fraction = fraction >> ((2 - (1ULL << j)) - exponent);
                        promoted.fraction <<= (i - temp.f_size);
                        promoted.f_size = i;
                    } else {
                        promoted.exponent = exponent + ((1 << j) - 1);
                        promoted.e_size = j;
                        promoted.fraction = fraction << (i - temp.f_size);
                        promoted.f_size = i;
                        promoted.fraction = promoted.fraction & ((1ULL << (i + 1)) - 1);
                    }
                }
            } else {
                promoted.fraction = temp.fraction << (i - temp.f_size);
                promoted.f_size = i;
            }

            // get the next unum in that environment for the generated unum
            temp2 = promoted;
            if (temp2.fraction != ((1ULL << (temp2.f_size + 1)) - 1)) {
                temp2.fraction++;
            } else {
                if (temp2.exponent != ((1 << (temp2.e_size + 1)) - 1)) {
                    temp2.fraction = 0;
                    temp2.exponent++;
                } else {
                    // This means the generated will border infinity since both exp
                    // and frac are maxed out
                    // simply continues since we have handled the cases with endpoints
                    // being infinity
                    continue;
                }
            }

            // if the other bound is equal to the generated one, match found
            if (left.sign) {
                if (isAbsValueEqual(temp2, left) && (right.sign || isZero(right))) {
                    promoted.ubit = 1;
                    promoted.sign = 1;
                    ub_result->right_bound = promoted;
                    ub_result->left_bound = promoted;
                    return;
                }
            } else {
                if (isAbsValueEqual(temp2, right) && !right.sign) {
                    promoted.ubit = 1;
                    ub_result->right_bound = promoted;
                    ub_result->left_bound = promoted;
                    return;
                }
            }
        }
    }

    // if no unification is found
    if (isNaNOrInf(left)) {
        left = _g_negopeninfu;
        if (isPosOrNegZero(right)) {
            right.sign = 1;
        }
    } else {
        if (left.sign) {
            if (left.fraction) {
                left.fraction--;
            } else if (left.exponent) {
                left.exponent--;
                left.fraction = (1ULL << (left.f_size + 1)) - 1;
            }

            if (isPosOrNegZero(right)) {
                right.sign = 1;
            }
        }
    }
    left.ubit = 1;

    if (isNaNOrInf(right)) {
        right = _g_posopeninfu;
    } else {
        if (!right.sign) {
            if (right.fraction) {
                right.fraction--;
            } else if (right.exponent) {
                right.exponent--;
                right.fraction = (1ULL << (right.f_size + 1)) - 1;
            }
        }
    }
    right.ubit = 1;

    ub_result->left_bound = left;
    ub_result->right_bound = right;

    return;
}

bool is_bordering_inf(unum_t u)
{
    if (!u.ubit)
        return false;

    if (u.exponent != ((1 << (u.e_size + 1)) - 1))
        return false;

    if (u.fraction != ((1ULL << (u.f_size + 1)) - 1) && !isPosOrNegInexactMaxreal(u))
        return false;

    return true;
}

bool isAbsBitsEqual(unum_t x, unum_t y)
{
    if (x.f_size != y.f_size)
        return false;

    if (x.e_size != y.e_size)
        return false;

    if (x.fraction != y.fraction)
        return false;

    if (x.exponent != y.exponent)
        return false;

    return true;
}

bool isPosOrNegZero(unum_t u)
{
    return !(u.exponent | u.fraction | u.ubit);
}

bool isUnumInArray(unum_temp_t* array, int array_size, unum_temp_t key)
{
    int j;
    for (j = 0; j < array_size; j++) {
        if (unum_compare(key.u, array[j].u) && key.is_open == array[j].is_open)
            return true;
    }
    return false;
}

bool isLessThanOrEqualToZero(unum_t u)
{
    if (!u.sign && (u.exponent || u.fraction))
        return false;

    return true;
}

bool isGreaterThanOrEqualToZero(unum_t u)
{
    if (u.sign && (u.exponent || u.fraction))
        return false;

    return true;
}

void get_gbound_from_unum(gbound_t* result, unum_t* u)
{

    result->left_bound = *u;
    result->right_bound = *u;
    result->left_open = 0;
    result->right_open = 0;

    if ((*u).ubit == 1) {
        // if NaN, set result to NaN and return
        if (isNaN(*u)) {
            (*result).left_bound = _g_qNaNu;
            (*result).right_bound = _g_qNaNu;
            return;
        }

        // if the unum is bordering infinity, set one of the boundaries as inf
        // if not, get the next biggest unum for one of the boundaries
        if ((*u).exponent == ((1 << ((*u).e_size + 1)) - 1) && (*u).fraction == ((1ULL << ((*u).f_size + 1)) - 1)) {
            if ((*u).sign == 0) {
                result->left_bound.ubit = 0;
                result->left_open = 1;
                result->right_bound = _g_posinfu;
                result->right_open = 1;
            } else {
                result->right_bound.ubit = 0;
                result->right_open = 1;
                result->left_bound = _g_neginfu;
                result->left_open = 1;
            }
        } else {
            if ((*u).sign == 0) {
                result->left_bound.ubit = 0;
                result->left_open = 1;
                result->right_bound.ubit = 0;
                result->right_open = 1;

                // if 1 can be added to fraction, add 1. if not add 1 to the exponent
                if (result->right_bound.fraction < ((1ULL << (result->right_bound.f_size + 1)) - 1)) {
                    result->right_bound.fraction++;
                } else {
                    result->right_bound.fraction = 0;
                    result->right_bound.exponent++;
                }
            } else {
                result->left_bound.ubit = 0;
                result->left_open = 1;
                result->right_bound.ubit = 0;
                result->right_open = 1;

                if (result->left_bound.fraction < ((1ULL << (result->left_bound.f_size + 1)) - 1)) {
                    result->left_bound.fraction++;
                } else {
                    result->left_bound.fraction = 0;
                    result->left_bound.exponent++;
                }
            }
        }
    }

    if (isPosOrNegZero(result->left_bound))
        result->left_bound.sign = 0;
    if (isPosOrNegZero(result->right_bound))
        result->right_bound.sign = 0;
    return;
}

void get_gbound_from_ubound(gbound_t* result, ubound_t* ub)
{

    result->left_bound = (*ub).left_bound;
    result->right_bound = (*ub).right_bound;

    if (isNaN(result->left_bound) || isNaN(result->right_bound)) {
        result->left_bound = _g_qNaNu;
        result->right_bound = _g_qNaNu;
    }

    if (!result->left_bound.ubit) {
        result->left_open = 0;
    } else {
        // if the number is bordering infinity
        if ((result->left_bound).sign &&
            (result->left_bound).exponent == ((1 << ((result->left_bound).e_size + 1)) - 1) &&
            (result->left_bound).fraction == ((1ULL << ((result->left_bound).f_size + 1)) - 1)) {
            result->left_bound = _g_neginfu;
            result->left_open = 1;
        } else {
            result->left_bound.ubit = 0;
            result->left_open = 1;
            if (result->left_bound.sign) {
                if (result->left_bound.fraction < ((1ULL << (result->left_bound.f_size + 1)) - 1)) {
                    result->left_bound.fraction++;
                } else {
                    result->left_bound.fraction = 0;
                    result->left_bound.exponent++;
                }
            }
        }
    }

    if (!result->right_bound.ubit) {
        result->right_open = 0;
    } else {
        // if the number is bordering infinity
        if (!(result->right_bound).sign &&
            (result->right_bound).exponent == ((1 << ((result->right_bound).e_size + 1)) - 1) &&
            (result->right_bound).fraction == ((1ULL << ((result->right_bound).f_size + 1)) - 1)) {
            result->right_bound = _g_posinfu;
            result->right_open = 1;
        } else {
            result->right_bound.ubit = 0;
            result->right_open = 1;
            if (!result->right_bound.sign) {
                if (result->right_bound.fraction < ((1ULL << (result->right_bound.f_size + 1)) - 1)) {
                    result->right_bound.fraction++;
                } else {
                    result->right_bound.fraction = 0;
                    result->right_bound.exponent++;
                }
            }
        }
    }
}

void get_ubound_result_from_gbound(ubound_t* result, gbound_t* gb)
{

    // if the left bound ubit is set
    if ((*gb).left_bound.ubit) {
        if (is_bordering_inf((*gb).left_bound) && (*gb).left_bound.sign) {
            (*gb).left_bound = _g_neginfu;
        } else {
            if ((*gb).left_bound.sign) {
                if ((*gb).left_bound.fraction != ((1ULL << ((*gb).left_bound.f_size + 1)) - 1))
                    (*gb).left_bound.fraction++;
                else {
                    (*gb).left_bound.exponent++;
                    (*gb).left_bound.fraction = 0;
                }
            }
        }

        (*gb).left_bound.ubit = 0;
        (*gb).left_open = 1;
    }

    // if the right bound ubit is set
    if ((*gb).right_bound.ubit) {
        if (is_bordering_inf((*gb).right_bound) && !(*gb).right_bound.sign) {
            (*gb).right_bound = _g_posinfu;
        } else {
            if (!(*gb).left_bound.sign) {
                if ((*gb).right_bound.fraction != ((1ULL << ((*gb).right_bound.f_size + 1)) - 1))
                    (*gb).right_bound.fraction++;
                else {
                    (*gb).right_bound.exponent++;
                    (*gb).right_bound.fraction = 0;
                }
            }
        }
        (*gb).right_open = 1;
        (*gb).right_bound.ubit = 0;
    }

    // if both the left and right are the same, then simply return them
    if (unum_compare((*gb).left_bound, (*gb).right_bound) && (*gb).left_open == (*gb).right_open) {

        if (isNaNOrInf((*gb).left_bound) && (*gb).left_open) {
            if ((*gb).left_bound.sign)
                result->left_bound = _g_minrealu;
            else
                result->left_bound = _g_maxrealu;

            result->left_bound.ubit = 1;
            result->right_bound = result->left_bound;
            return;
        }

        if ((*gb).left_open)
            (*gb).left_bound.ubit = 1;
        result->left_bound = (*gb).left_bound;
        result->right_bound = result->left_bound;
        return;
    } else {
#ifdef UNIFY
        unify_and_get_result(&(*result), *gb);
#else
        // with no unification
        if (!(*gb).left_open) {
            result->left_bound = (*gb).left_bound;
        } else {
            if (isNaNOrInf((*gb).left_bound)) {
                (*gb).left_bound = _g_negopeninfu;
            } else {
                if ((*gb).left_bound.sign) {
                    if ((*gb).left_bound.fraction) {
                        (*gb).left_bound.fraction--;
                    } else {
                        (*gb).left_bound.exponent--;
                        (*gb).left_bound.fraction = (1ULL << ((*gb).left_bound.f_size + 1)) - 1;
                    }
                }
            }

            (*gb).left_bound.ubit = 1;
            result->left_bound = (*gb).left_bound;
        }

        if (!(*gb).right_open) {
            result->right_bound = (*gb).right_bound;
        } else {
            if (isNaNOrInf((*gb).right_bound)) {
                (*gb).right_bound = _g_posopeninfu;
            } else {
                if (!(*gb).right_bound.sign) {
                    if ((*gb).right_bound.fraction) {
                        (*gb).right_bound.fraction--;
                    } else {
                        (*gb).right_bound.exponent--;
                        (*gb).right_bound.fraction = (1ULL << ((*gb).right_bound.f_size + 1)) - 1;
                    }
                }
            }
            (*gb).right_bound.ubit = 1;
            result->right_bound = (*gb).right_bound;
        }

#endif
    }

    return;
}

void negate_ubound(ubound_t* ub)
{
    unum_t temp;
    temp = (*ub).left_bound;
    (*ub).left_bound = (*ub).right_bound;
    (*ub).right_bound = temp;

    if (!isZero((*ub).left_bound))
        (*ub).left_bound.sign = !(*ub).left_bound.sign;

    if (!isZero((*ub).right_bound))
        (*ub).right_bound.sign = !(*ub).right_bound.sign;

    return;
}

void unum_reset_bit_track_counts()
{
    total_bit_count = 0;
    total_op_count = 0;
    max_bit_count = 0;
    min_bit_count = _g_maxubits * 2;
    return;
}
