#include "libunum.h"

void squareu(ubound_t* result, unum_t* op)
{
    gbound_t g_op;
    unum_t t1, t2;

    if (isNaN(*op)) {
        (*result).left_bound = _g_qNaNu;
        (*result).right_bound = _g_qNaNu;
        return;
    }

    if (isPosOrNegZero(*op)) {
        (*result).left_bound = _g_zero;
        (*result).right_bound = _g_zero;
        return;
    }

    if (!(*op).ubit) {
        unum_mul(&t1, op, op);
        (*result).left_bound = t1;
        (*result).right_bound = (*result).left_bound;
        return;
    }

    get_gbound_from_unum(&g_op, op);

    // A single inexact unum cannot span zero nor contain it.
    // Therefore simply square the two endpoints
    if (isPosOrNegZero(g_op.left_bound)) {
        t1 = _g_zero;
    } else {
        unum_mul(&t1, &g_op.left_bound, &g_op.left_bound);
    }

    if (isPosOrNegZero(g_op.right_bound)) {
        t2 = _g_zero;
    } else {
        unum_mul(&t2, &g_op.right_bound, &g_op.right_bound);
    }

    if ((*op).sign) {
        g_op.left_bound = t2;
        g_op.right_bound = t1;
    } else {
        g_op.left_bound = t1;
        g_op.right_bound = t2;
    }

    get_ubound_result_from_gbound(result, &g_op);
    return;
}

void squareubound(ubound_t* result, ubound_t* op)
{
    gbound_t g_op, gbound_result;
    unum_t t1, t2;

    if (isNaN((*op).left_bound) || isNaN((*op).right_bound)) {
        (*result).left_bound = _g_qNaNu;
        (*result).right_bound = _g_qNaNu;
        return;
    }

    if (unum_compare((*op).left_bound, (*op).right_bound)) {
        squareu(result, &(*op).left_bound);
        return;
    }

    get_gbound_from_ubound(&g_op, op);

    if (isPosOrNegZero(g_op.left_bound)) {
        t1 = _g_zero;
    } else {
        unum_mul(&t1, &g_op.left_bound, &g_op.left_bound);
    }

    if (isPosOrNegZero(g_op.right_bound)) {
        t2 = _g_zero;
    } else {
        unum_mul(&t2, &g_op.right_bound, &g_op.right_bound);
    }

    if ((isLessThanZero(g_op.left_bound) && isLessThanZero(g_op.right_bound)) || isPosOrNegZero(g_op.right_bound)) {
        gbound_result.left_bound = t2;
        gbound_result.right_bound = t1;
        gbound_result.left_open = g_op.right_open;
        gbound_result.right_open = g_op.left_open;
        get_ubound_result_from_gbound(result, &gbound_result);
        return;
    }

    if ((isGreaterThanZero(g_op.left_bound) && isGreaterThanZero(g_op.right_bound)) ||
        isPosOrNegZero(g_op.left_bound)) {
        gbound_result.left_bound = t1;
        gbound_result.right_bound = t2;
        gbound_result.left_open = g_op.left_open;
        gbound_result.right_open = g_op.right_open;
        get_ubound_result_from_gbound(result, &gbound_result);
        return;
    }

    if (isLessThanZero(g_op.left_bound) && isGreaterThanZero(g_op.right_bound)) {
        if (isAbsBitsEqual(g_op.left_bound, g_op.right_bound)) {
            gbound_result.left_bound = _g_zero;
            gbound_result.right_bound = t1;
            gbound_result.left_open = 0;
            gbound_result.right_open = g_op.left_open & g_op.right_open;
        } else {
            gbound_result.left_bound = _g_zero;
            gbound_result.left_open = 0;
            // Not the extensible way to do it, remove this later
            double t1_d = u2f(g_op.left_bound);
            double t2_d = u2f(g_op.right_bound);
            if (t1_d * t1_d > t2_d * t2_d) {
                gbound_result.right_bound = t1;
                gbound_result.right_open = g_op.left_open;
            } else {
                gbound_result.right_bound = t2;
                gbound_result.right_open = g_op.right_open;
            }
        }
        get_ubound_result_from_gbound(result, &gbound_result);
        return;
    }
}
