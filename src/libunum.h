/*
 * Header file for unum functions
 */
//unum type

#ifndef LIBUNUM_H
#define LIBUNUM_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>

typedef int bool;
#define false 0
#define true 1

#define INT_T

//we're using int as the intermediate type when calculating the exponent
#define INTERMEDIATE_EXP_SIZE 32
#define INTERMEDIATE_FRACTION_SIZE 64

#define SINGLE_PREC_BIAS 127
#define SINGLE_PREC_F_SIZE 23
#define DOUBLE_PREC_BIAS 1023
#define DOUBLE_PREC_F_SIZE 52
#define INT_SIZE 32

//colours
#define RESET "\033[0m"
#define KBLK "\x1B[30m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"

#define IS_ODD(n) (n % 2)
#define IS_POWER_OF_2(n) ((n != 0) && ((n & (n - 1)) == 0))

typedef struct unum_s {
    bool sign;
    unsigned int exponent;
    unsigned long long fraction;
    bool ubit;
    unsigned short e_size;
    unsigned short f_size;
} unum_t;

typedef union {
    double f;

    struct {
        unsigned long long fraction : 52;
        unsigned int exponent : 11;
        unsigned int sign : 1;
    } parts;
} double_cast;

typedef struct ubound_s {
    unum_t left_bound;
    unum_t right_bound;
} ubound_t;

typedef struct unum_temp_s {
    unum_t u;
    bool is_open;
} unum_temp_t;

typedef struct gbound_s {
    unum_t left_bound;
    bool left_open;
    unum_t right_bound;
    bool right_open;
} gbound_t;

//globals
int _g_esizesize;
int _g_fsizesize;
int _g_esizemax;
int _g_fsizemax;
int _g_utagsize;
int _g_maxubits;
int _g_maxexpval;
unsigned long long _g_maxfracval;
unsigned long long _g_maxfracvalhidden;
unum_t _g_utagmask;
unum_t _g_ulpu;
unum_t _g_smallsubnormalu;
unum_t _g_smallnormalu;
unum_t _g_signbigu;
unum_t _g_posinfu;
unum_t _g_maxrealu;
unum_t _g_minrealu;
unum_t _g_neginfu;
unum_t _g_negbigu;
unum_t _g_qNaNu;
unum_t _g_sNaNu;
unum_t _g_negopeninfu;
unum_t _g_posopeninfu;
unum_t _g_negopenzerou;
unum_t _g_zero;
unum_t _g_negzero;
double _g_maxreal;
double _g_smallnormal;
double _g_smallsubnormal;
int _g_maxexp;
int _g_minexp;

//for bit tracking
unsigned long long total_bit_count;
unsigned long long total_op_count;
unsigned int max_bit_count;
unsigned int min_bit_count;

/*
 *Initializes a unum
 */
void unum_init(unum_t *x);

/*
 * Initialize unum environment and its global variables
 */
void set_env(int e_sizesize, int f_sizesize);

/*
 * print the integer values of the unum
 * (somewhat similar to uview of the prototype)
 */
void printu(unum_t u);

/*
 * Convert a unum to single precision float value
 */
double u2f(unum_t u);

/*
 * Compare two unums for equality
 */
bool unum_compare(unum_t x, unum_t y);

/*
 * Converts a single precision float to a unum
 */
void x2u(double f, unum_t *u);

/*
 * Converts a single precision float to a ubound
 */
void x2ub(double f, ubound_t *ub);

/*
 * print bits of an unsigned long long
 */
void print_bits(unsigned long long x, char* colour, unsigned short width);

/*
 * View the unum
 * (Similar to unumview of the prototype)
 */
void unumview(unum_t x);

/*
 * Multiply two unums
 */
void unum_mul(unum_t *result, unum_t *op1, unum_t *op2);

/*
 *Initializes a ubound
 */
void ubound_init(ubound_t *ub);

/*
 *Initializes a ubound from a single unum
 */
void ubound_from_unum_init(ubound_t *ub, unum_t u);

/*
 * The "left" multiplication table for general intervals
 */
void timesposleft(gbound_t *result, unum_t x, bool xb, unum_t y, bool yb);

/*
 * The "right" multiplication table for general intervals
 */
void timesposright(gbound_t *result, unum_t x, bool xb, unum_t y, bool yb);

/*
 * Rigourously multiplies two ubounds
 */
void timesg(gbound_t *result, gbound_t op1, gbound_t op2);

/*
 * Multiplies two unums
 */
void timesu(ubound_t *result, unum_t op1, unum_t op2);

/*
 * Multiplies two ubounds
 */
void timesubound(ubound_t *result, ubound_t op1, ubound_t op2);

/*
 * Check if unum provided as key is in the given array
 */
bool isUnumInArray(unum_temp_t *array, int array_size, unum_temp_t key);

/*
 * View the bits of a ubound
 */
void uboundview(ubound_t ub);

/*
 * Returns true if the unum is NaN, false otherwise
 */
bool isNaN(unum_t u);

/*
 * Returns true if the unum is negative or positive infinity,
 * false otherwise
 */
bool isInf(unum_t u);

/*
 * Returns true if the unum is negative infinity, false otherwise
 */
bool isNegInf(unum_t u);

/*
 * Returns true if the unum is positive infinity, false otherwise
 */
bool isPosInf(unum_t u) ;

/*
 * Returns true if the unum is NaN or negative or positive infinity,
 * false otherwise
 */
bool isNaNOrInf(unum_t u);

/*
 * Returns true if the unum is zero, false otherwise
 */
bool isZero(unum_t u);

/*
 * Returns true if the unum value is less than zero, false otherwise
 */
bool isLessThanZero(unum_t u);

/*
 * Returns true if the unum value is NaN or plus or minus Inf,
 * false otherwise
 */
bool isNaNOrInf(unum_t u);

/*
 * Returns true if the unum value is greater than zero,
 * false otherwise
 */
bool isGreaterThanZero(unum_t u);

/*
 * Returns true if the unum value is plus or minus inexact maxrealu,
 * false otherwise
 */
bool isPosOrNegInexactMaxreal(unum_t u);

/*
 * Returns true if the absolute value of x & y are equal, false
 * otherwise
 */
bool isAbsValueEqual(unum_t x, unum_t y);

/*
 * Returns true if the value is positive or negative maxreal,
 * false otherwise
 */
bool isPosOrNegMaxreal(unum_t u);

/*
 * Takes in a gbound and returns a ubound with possible unification
 */
void unify_and_get_result(ubound_t *ub_result, gbound_t gbound);

/*
 * Returns true if the unum is bordering infinity, false otherwise
 */
bool is_bordering_inf(unum_t u);

/*
 * Returns true if all bits except the sign bit are equal, false
 * otherwise
 */
bool isAbsBitsEqual(unum_t x, unum_t y);

/*
 * Returns true if the value is positive or negative zero, false
 * otherwise
 */
bool isPosOrNegZero(unum_t u);

/*
 * Divides two unums
 */
void unum_div(unum_t *result, unum_t *op1, unum_t *op2);

/*
 * The "left" division table for general intervals
 */
void divideposleft(unum_temp_t *result, unum_t x, bool xb, unum_t y, bool yb);

/*
 * The "right" division table for general intervals
 */
void divideposright(unum_temp_t *result, unum_t x, bool xb, unum_t y, bool yb);

/*
 * Rigourously divides two gbounds
 */
void divideg(gbound_t *result, gbound_t op1, gbound_t op2);

/*
 * Returns true if the unum is less than or equal to zero, false otherwise
 */
bool isLessThanOrEqualToZero(unum_t u);

/*
 * Returns true if the unum is greater than or equal to zero, false otherwise
 */
bool isGreaterThanOrEqualToZero(unum_t u);

/*
 * Divides two unums
 */
void divideu(ubound_t *result, unum_t op1, unum_t op2);

/*
 * Divides two ubounds
 */
void divideubound(ubound_t *result, ubound_t op1, ubound_t op2);

/*
 * Find the squareroot of a unum
 */
void sqrtu(ubound_t *result, unum_t *op);

/*
 * Returns the square root of a unsigned long
 */
unsigned long long sqrt32(unsigned long long n);

/*
 * Find the squareroot of an exact unum
 */
void unum_sqrt(unum_t *result, unum_t *u);

/*
 * Find the square root of a general ubound
 */
void sqrtubound(ubound_t *result, ubound_t *op);

/*
 * Forms a gbound from a given unum
 */
void get_gbound_from_unum(gbound_t *result, unum_t *u);

/*
 * Forms a gbound from a given ubound
 */
void get_gbound_from_ubound(gbound_t *result, ubound_t *ub);

/*
 * Forms a ubound result from a given gbound
 */
void get_ubound_result_from_gbound(ubound_t *result, gbound_t *gb);

/*
 * Add two exact unums
 */
void unum_add(unum_t* result, unum_t* op1, unum_t* op2);

/*
 * Add two general gbounds
 */
void plusg(gbound_t *result, gbound_t op1, gbound_t op2);

/*
 * Add two general unums
 */
void plusu(ubound_t* result, unum_t op1, unum_t op2);

/*
 * Add two general ubounds
 */
void plusubound(ubound_t* result, ubound_t op1, ubound_t op2);

/*
 * Subtract two general unums
 */
void minusu(ubound_t* result, unum_t op1, unum_t op2);

/*
 * Subtract two general ubounds
 */
void minusubound(ubound_t* result, ubound_t op1, ubound_t op2);

/*
 * Negate a ubound
 */
void negate_ubound(ubound_t* ub);

/*
 * Reset bit tracking counts
 */
void unum_reset_bit_track_counts();

/*
 * Calculate the square of a unum
 */
void squareu(ubound_t* result, unum_t* op);

/*
 * Calculate the square of a ubound
 */
void squareubound(ubound_t* result, ubound_t* op);

/*
 * Perform a fused dot product of two lists of unums and return the
 * ubound result
 */
void fused_unum_dot_product(ubound_t* result, int size, unum_t list_a[], unum_t list_b[]);

/*
 * Perform a fused dot product of two lists of ubounds and return the
 * ubound result
 */
void fused_ubound_dot_product(ubound_t* result, int size, ubound_t list_a[], ubound_t list_b[]);

/*
 * Fused sum of a list of unums
 */
void fused_unum_add(ubound_t* result, int size, unum_t list_a[]);

/*
 * Fused sum of a list of ubounds
 */
void fused_ubound_add(ubound_t* result, int size, ubound_t list_a[]);

#endif
