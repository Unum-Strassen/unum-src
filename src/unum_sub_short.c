#include "libunum.h"

void minusu(ubound_t* result, unum_t op1, unum_t op2) {
	if(isNaN(op1) || isNaN(op2)) {
		(*result).left_bound = _g_qNaNu;
		(*result).right_bound = _g_qNaNu;
		return;
	}

	op2.sign = !op2.sign;
	plusu(result, op1, op2);
	return;
}

void minusubound(ubound_t* result, ubound_t op1, ubound_t op2) {

	ubound_t temp;

	if(isNaN(op1.left_bound) || isNaN(op1.right_bound)
			|| isNaN(op2.left_bound) || isNaN(op2.right_bound)) {
		(*result).left_bound = _g_qNaNu;
		(*result).right_bound = _g_qNaNu;
		return;
	}

	op2.left_bound.sign = !op2.left_bound.sign;
	op2.right_bound.sign = !op2.right_bound.sign;
	temp.left_bound = op2.right_bound;
	temp.right_bound = op2.left_bound;

	plusubound(result, op1, temp);
	return;
}

