#include "libunum.h"
#include "libunum_fused.h"

void fused_unum_dot_product(ubound_t* result, int size, unum_t list_a[],
		unum_t list_b[]) {
	int i;
	fused_inter_pair_t multiplied_list[size];
	gbound_t gbound_result, gbound_a, gbound_b;
	//maximum f_size after multiplication
	int fraction_shift = (1 << (_g_fsizesize + 1));
	int bias = (1 << _g_esizemax) - 4;
	fused_inter_t left_sum, right_sum;

	//initialize
	mpz_init(left_sum.fraction);
	mpz_init(right_sum.fraction);
	left_sum.isInf = 0;
	right_sum.isInf = 0;
	left_sum.ubit = 0;
	right_sum.ubit = 0;
	left_sum.sign = 0;
	right_sum.sign = 0;

	//multiplication
	for (i = 0; i < size; i++) {
		if (list_a[i].ubit == 0 && list_b[i].ubit == 0
				&& !(isInf(list_a[i]) || isInf(list_b[i]))) {
			//if both numbers are exact, simply multiply
			unum_mul_fused(&multiplied_list[i].left, list_a[i], list_b[i]);

			//copy everything to the right
			multiplied_list[i].right.sign = multiplied_list[i].left.sign;
			multiplied_list[i].right.ubit = multiplied_list[i].left.ubit;
			multiplied_list[i].right.exponent =
					multiplied_list[i].left.exponent;
			multiplied_list[i].right.f_size = multiplied_list[i].left.f_size;
			multiplied_list[i].right.isInf = multiplied_list[i].left.isInf;
			mpz_init(multiplied_list[i].right.fraction);
			mpz_set(multiplied_list[i].right.fraction,
					multiplied_list[i].left.fraction);
		} else {
			//if either the unums are inexact (or a special case), use timesg
			get_gbound_from_unum(&gbound_a, &list_a[i]);
			get_gbound_from_unum(&gbound_b, &list_b[i]);

			//TODO check for NaNs
			timesg_fused(&multiplied_list[i], gbound_a, gbound_b);
		}

		//make the fraction sizes the same, in preparation for addition
		mpz_mul_2exp(multiplied_list[i].left.fraction,
				multiplied_list[i].left.fraction,
				(fraction_shift - multiplied_list[i].left.f_size));
		mpz_mul_2exp(multiplied_list[i].right.fraction,
				multiplied_list[i].right.fraction,
				(fraction_shift - multiplied_list[i].right.f_size));

		//make the exponents the same
		mpz_mul_2exp(multiplied_list[i].left.fraction,
				multiplied_list[i].left.fraction,
				(multiplied_list[i].left.exponent + bias));
		mpz_mul_2exp(multiplied_list[i].right.fraction,
				multiplied_list[i].right.fraction,
				(multiplied_list[i].right.exponent + bias));

		//addition
		//add the fractions while checking for infinities
		//left endpoint
		if (!multiplied_list[i].left.isInf && !left_sum.isInf) {
			if (multiplied_list[i].left.sign) {
				mpz_neg(multiplied_list[i].left.fraction,
						multiplied_list[i].left.fraction);
			}
			mpz_add(left_sum.fraction, left_sum.fraction,
					multiplied_list[i].left.fraction);
			left_sum.ubit = left_sum.ubit | multiplied_list[i].left.ubit;
			if (mpz_sgn(left_sum.fraction) < 0) {
				left_sum.sign = 1;
			} else {
				left_sum.sign = 0;
			}
		} else if ((multiplied_list[i].left.isInf
				&& !multiplied_list[i].left.ubit && multiplied_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && left_sum.sign)) {
			if ((multiplied_list[i].left.isInf && !multiplied_list[i].left.ubit
					&& !multiplied_list[i].left.sign)
					|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
				printf("Result is NaN in left endpoint sum\n");
			} else {
				mpz_set_ui(left_sum.fraction, 0);
				left_sum.sign = 1;
				left_sum.ubit = 0;
				left_sum.isInf = 1;
			}
		} else if ((multiplied_list[i].left.isInf
				&& !multiplied_list[i].left.ubit
				&& !multiplied_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 0;
			left_sum.ubit = 0;
			left_sum.isInf = 1;
		} else if ((multiplied_list[i].left.isInf
				&& multiplied_list[i].left.ubit && multiplied_list[i].left.sign)
				|| (left_sum.isInf && left_sum.ubit && left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 1;
			left_sum.ubit = 1;
			left_sum.isInf = 1;
		}
		mpz_clear(multiplied_list[i].left.fraction);

		//right endpoint
		if (!multiplied_list[i].right.isInf && !right_sum.isInf) {
			if (multiplied_list[i].right.sign)
				mpz_neg(multiplied_list[i].right.fraction,
						multiplied_list[i].right.fraction);
			mpz_add(right_sum.fraction, right_sum.fraction,
					multiplied_list[i].right.fraction);
			right_sum.ubit = right_sum.ubit | multiplied_list[i].right.ubit;
			if (mpz_sgn(right_sum.fraction) < 0)
				right_sum.sign = 1;
			else
				right_sum.sign = 0;
		} else if ((multiplied_list[i].right.isInf
				&& !multiplied_list[i].right.ubit
				&& multiplied_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && right_sum.sign)) {
			if ((multiplied_list[i].right.isInf
					&& !multiplied_list[i].right.ubit
					&& !multiplied_list[i].right.sign)
					|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
				printf("Result is NaN in right endpoint sum\n");
			} else {
				mpz_set_ui(right_sum.fraction, 0);
				right_sum.sign = 1;
				right_sum.ubit = 0;
				right_sum.isInf = 1;
			}
		} else if ((multiplied_list[i].right.isInf
				&& !multiplied_list[i].right.ubit
				&& !multiplied_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 0;
			right_sum.isInf = 1;
		} else if ((multiplied_list[i].right.isInf
				&& multiplied_list[i].right.ubit
				&& !multiplied_list[i].right.sign)
				|| (right_sum.isInf && right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 1;
			right_sum.isInf = 1;
		}
		mpz_clear(multiplied_list[i].right.fraction);
	}

	//convert into a ubound
	//left
	int fraction_length = 0, trailing_zeros = 0, ubit = 0;
	unsigned long long added_fractions = 0;
	int truncation_amount = 0;
	if (!left_sum.isInf) {
		//get 64 bits of the fraction and fraction bit length
		if (mpz_sgn(left_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(left_sum.fraction, 2);
			trailing_zeros = mpz_scan1(left_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						fraction_shift, left_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, fraction_shift,
						left_sum.sign);
			}
			gbound_result.left_open = left_sum.ubit;
		} else {
			gbound_result.left_bound = _g_zero;
			gbound_result.left_open = left_sum.ubit;
		}
	} else if (left_sum.isInf && left_sum.ubit == 0) {
		gbound_result.left_bound = _g_posinfu;
		gbound_result.left_open = 0;
		if (left_sum.sign)
			gbound_result.left_bound.sign = 1;
	} else {
		gbound_result.left_bound = _g_neginfu;
		gbound_result.left_open = 1;
	}

	//right
	if (!right_sum.isInf) {
		if (mpz_sgn(right_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(right_sum.fraction, 2);
			trailing_zeros = mpz_scan1(right_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						fraction_shift, right_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, fraction_shift,
						right_sum.sign);
			}
			gbound_result.right_open = right_sum.ubit;
		} else {
			gbound_result.right_bound = _g_zero;
			gbound_result.right_open = right_sum.ubit;
		}
	} else if (right_sum.isInf && right_sum.ubit == 0) {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 0;
		if (right_sum.sign)
			gbound_result.right_bound.sign = 1;
	} else {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 1;
	}

	//get ubound result from gbound
	get_ubound_result_from_gbound(result, &gbound_result);

	//release all the previously initialized mpz_t values
	mpz_clear(left_sum.fraction);
	mpz_clear(right_sum.fraction);
	return;
}

void fused_ubound_dot_product(ubound_t* result, int size, ubound_t list_a[],
		ubound_t list_b[]) {

	fused_inter_pair_t multiplied_list[size];
	int i;
	gbound_t gbound_result, gbound_a, gbound_b;
	int fraction_shift = (1 << (_g_fsizesize + 1));
	int bias = (1 << _g_esizemax) - 4;
	fused_inter_t left_sum, right_sum;

	//initialize
	mpz_init(left_sum.fraction);
	mpz_init(right_sum.fraction);
	left_sum.isInf = 0;
	right_sum.isInf = 0;
	left_sum.ubit = 0;
	right_sum.ubit = 0;
	left_sum.sign = 0;
	right_sum.sign = 0;

	for (i = 0; i < size; i++) {
		get_gbound_from_ubound(&gbound_a, &list_a[i]);
		get_gbound_from_ubound(&gbound_b, &list_b[i]);

		timesg_fused(&multiplied_list[i], gbound_a, gbound_b);

		//make the fraction sizes the same, in preparation for addition
		mpz_mul_2exp(multiplied_list[i].left.fraction,
				multiplied_list[i].left.fraction,
				(fraction_shift - multiplied_list[i].left.f_size));
		mpz_mul_2exp(multiplied_list[i].right.fraction,
				multiplied_list[i].right.fraction,
				(fraction_shift - multiplied_list[i].right.f_size));

		//make the exponents the same
		mpz_mul_2exp(multiplied_list[i].left.fraction,
				multiplied_list[i].left.fraction,
				(multiplied_list[i].left.exponent + bias));
		mpz_mul_2exp(multiplied_list[i].right.fraction,
				multiplied_list[i].right.fraction,
				(multiplied_list[i].right.exponent + bias));

		//addition
		//add the fractions while checking for infinities
		//left endpoint
		if (!multiplied_list[i].left.isInf && !left_sum.isInf) {
			if (multiplied_list[i].left.sign) {
				mpz_neg(multiplied_list[i].left.fraction,
						multiplied_list[i].left.fraction);
			}
			mpz_add(left_sum.fraction, left_sum.fraction,
					multiplied_list[i].left.fraction);
			left_sum.ubit = left_sum.ubit | multiplied_list[i].left.ubit;
			if (mpz_sgn(left_sum.fraction) < 0) {
				left_sum.sign = 1;
			} else {
				left_sum.sign = 0;
			}
		} else if ((multiplied_list[i].left.isInf
				&& !multiplied_list[i].left.ubit && multiplied_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && left_sum.sign)) {
			if ((multiplied_list[i].left.isInf && !multiplied_list[i].left.ubit
					&& !multiplied_list[i].left.sign)
					|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
				printf("Result is NaN in left endpoint sum\n");
			} else {
				mpz_set_ui(left_sum.fraction, 0);
				left_sum.sign = 1;
				left_sum.ubit = 0;
				left_sum.isInf = 1;
			}
		} else if ((multiplied_list[i].left.isInf
				&& !multiplied_list[i].left.ubit
				&& !multiplied_list[i].left.sign)
				|| (left_sum.isInf && !left_sum.ubit && !left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 0;
			left_sum.ubit = 0;
			left_sum.isInf = 1;
		} else if ((multiplied_list[i].left.isInf
				&& multiplied_list[i].left.ubit && multiplied_list[i].left.sign)
				|| (left_sum.isInf && left_sum.ubit && left_sum.sign)) {
			mpz_set_ui(left_sum.fraction, 0);
			left_sum.sign = 1;
			left_sum.ubit = 1;
			left_sum.isInf = 1;
		}
		mpz_clear(multiplied_list[i].left.fraction);

		//right endpoint
		if (!multiplied_list[i].right.isInf && !right_sum.isInf) {
			if (multiplied_list[i].right.sign)
				mpz_neg(multiplied_list[i].right.fraction,
						multiplied_list[i].right.fraction);
			mpz_add(right_sum.fraction, right_sum.fraction,
					multiplied_list[i].right.fraction);
			right_sum.ubit = right_sum.ubit | multiplied_list[i].right.ubit;
			if (mpz_sgn(right_sum.fraction) < 0)
				right_sum.sign = 1;
			else
				right_sum.sign = 0;
		} else if ((multiplied_list[i].right.isInf
				&& !multiplied_list[i].right.ubit
				&& multiplied_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && right_sum.sign)) {
			if ((multiplied_list[i].right.isInf
					&& !multiplied_list[i].right.ubit
					&& !multiplied_list[i].right.sign)
					|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
				printf("Result is NaN in right endpoint sum\n");
			} else {
				mpz_set_ui(right_sum.fraction, 0);
				right_sum.sign = 1;
				right_sum.ubit = 0;
				right_sum.isInf = 1;
			}
		} else if ((multiplied_list[i].right.isInf
				&& !multiplied_list[i].right.ubit
				&& !multiplied_list[i].right.sign)
				|| (right_sum.isInf && !right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 0;
			right_sum.isInf = 1;
		} else if ((multiplied_list[i].right.isInf
				&& multiplied_list[i].right.ubit
				&& !multiplied_list[i].right.sign)
				|| (right_sum.isInf && right_sum.ubit && !right_sum.sign)) {
			mpz_set_ui(right_sum.fraction, 0);
			right_sum.sign = 0;
			right_sum.ubit = 1;
			right_sum.isInf = 1;
		}
		mpz_clear(multiplied_list[i].right.fraction);
	}

	//convert into a ubound
	//left
	int fraction_length = 0, trailing_zeros = 0, ubit = 0;
	unsigned long long added_fractions = 0;
	int truncation_amount = 0;
	if (!left_sum.isInf) {
		//get 64 bits of the fraction and fraction bit length
		if (mpz_sgn(left_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(left_sum.fraction, 2);
			trailing_zeros = mpz_scan1(left_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						fraction_shift, left_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(left_sum.fraction, left_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, left_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.left_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, fraction_shift,
						left_sum.sign);
			}
			gbound_result.left_open = left_sum.ubit;
		} else {
			gbound_result.left_bound = _g_zero;
			gbound_result.left_open = left_sum.ubit;
		}
	} else if (left_sum.isInf && left_sum.ubit == 0) {
		gbound_result.left_bound = _g_posinfu;
		gbound_result.left_open = 0;
		if (left_sum.sign)
			gbound_result.left_bound.sign = 1;
	} else {
		gbound_result.left_bound = _g_neginfu;
		gbound_result.left_open = 1;
	}

	//right
	if (!right_sum.isInf) {
		if (mpz_sgn(right_sum.fraction) != 0) {
			fraction_length = mpz_sizeinbase(right_sum.fraction, 2);
			trailing_zeros = mpz_scan1(right_sum.fraction, 0);
			if ((fraction_length - trailing_zeros) > 64) {
				truncation_amount = (fraction_length - 64);
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 1;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias, 64,
						fraction_shift, right_sum.sign);
			} else {
				truncation_amount = trailing_zeros;
				mpz_tdiv_q_2exp(right_sum.fraction, right_sum.fraction,
						truncation_amount);
				mpz_export(&added_fractions, 0, -1, sizeof added_fractions, 0,
						0, right_sum.fraction);
				ubit = 0;
				get_unum_for_fused_inter_t(&gbound_result.right_bound,
						added_fractions, ubit, truncation_amount - bias,
						fraction_length - trailing_zeros, fraction_shift,
						right_sum.sign);
			}
			gbound_result.right_open = right_sum.ubit;
		} else {
			gbound_result.right_bound = _g_zero;
			gbound_result.right_open = right_sum.ubit;
		}
	} else if (right_sum.isInf && right_sum.ubit == 0) {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 0;
		if (right_sum.sign)
			gbound_result.right_bound.sign = 1;
	} else {
		gbound_result.right_bound = _g_posinfu;
		gbound_result.right_open = 1;
	}

	//get ubound result from gbound
	get_ubound_result_from_gbound(result, &gbound_result);

	//release all the previously initialized mpz_t values
	mpz_clear(left_sum.fraction);
	mpz_clear(right_sum.fraction);
	return;
}

void timesg_fused(fused_inter_pair_t* result, gbound_t op1, gbound_t op2) {
	fused_inter_t lcan[4], rcan[4];
	unum_t op1_temp, op2_temp;
	int lcan_size = 0, rcan_size = 0;
	int i;

	// Lower left corner in upper right quadrant, facing uphill
	if (isGreaterThanOrEqualToZero(op1.left_bound)
			&& isGreaterThanOrEqualToZero(op2.left_bound)) {
		timesposleft_fused(&lcan[lcan_size], op1.left_bound, op1.left_open,
				op2.left_bound, op2.left_open);
		lcan_size++;
	}

	// Upper right corner in lower left quadrant, facing uphill
	if ((isLessThanZero(op1.right_bound)
			|| (isPosOrNegZero(op1.right_bound) && op1.right_open))
			&& (isLessThanZero(op2.right_bound)
					|| (isPosOrNegZero(op2.right_bound) && op2.right_open))) {
		op1_temp = op1.right_bound;
		if (!isPosOrNegZero(op1_temp) || op1.right_open)
			op1_temp.sign = !op1_temp.sign;
		op2_temp = op2.right_bound;
		if (!isPosOrNegZero(op2_temp) || op2.right_open)
			op2_temp.sign = !op2_temp.sign;
		timesposleft_fused(&lcan[lcan_size], op1_temp, op1.right_open, op2_temp,
				op2.right_open);
		lcan_size++;
	}

	// Upper left corner in upper left quadrant, facing uphill
	if ((isLessThanZero(op1.left_bound)
			|| (isPosOrNegZero(op1.left_bound) && !op1.left_open))
			&& (isGreaterThanZero(op2.right_bound)
					|| (isPosOrNegZero(op2.right_bound) && !op2.right_open))) {
		op1_temp = op1.left_bound;
		if (!isPosOrNegZero(op1_temp) || op1.left_open)
			op1_temp.sign = !op1_temp.sign;
		timesposright_fused(&lcan[lcan_size], op1_temp, op1.left_open,
				op2.right_bound, op2.right_open);
		if ((mpz_sgn(lcan[lcan_size].fraction) != 0)
				|| lcan[lcan_size].ubit == 1) {
			lcan[lcan_size].sign = !lcan[lcan_size].sign;
		}
		lcan_size++;
	}

	// Lower right corner in lower right quadrant, facing uphill
	if ((isGreaterThanZero(op1.right_bound)
			|| (isPosOrNegZero(op1.right_bound) && !op1.right_open))
			&& (isLessThanZero(op2.left_bound)
					|| (isPosOrNegZero(op2.left_bound) && !op2.left_open))) {
		op2_temp = op2.left_bound;
		if (!isPosOrNegZero(op2_temp) || op2.left_open)
			op2_temp.sign = !op2_temp.sign;
		timesposright_fused(&lcan[lcan_size], op1.right_bound, op1.right_open,
				op2_temp, op2.left_open);
		if ((mpz_sgn(lcan[lcan_size].fraction) != 0)
				|| lcan[lcan_size].ubit == 1) {
			lcan[lcan_size].sign = !lcan[lcan_size].sign;
		}
		lcan_size++;
	}

	// Upper right corner in upper right quadrant, facing downhill
	if ((isGreaterThanZero(op1.right_bound)
			|| (isPosOrNegZero(op1.right_bound) && !op1.right_open))
			&& (isGreaterThanZero(op2.right_bound)
					|| (isPosOrNegZero(op2.right_bound) && !op2.right_open))) {
		timesposright_fused(&rcan[rcan_size], op1.right_bound, op1.right_open,
				op2.right_bound, op2.right_open);
		rcan_size++;
	}

	// Lower left corner in lower left quadrant, facing downhill
	if ((isLessThanZero(op1.left_bound)
			|| (isPosOrNegZero(op1.left_bound) && !op1.left_open))
			&& (isLessThanZero(op2.left_bound)
					|| (isPosOrNegZero(op2.left_bound) && !op2.left_open))) {
		op1_temp = op1.left_bound;
		if (!isPosOrNegZero(op1_temp) || op1.left_open)
			op1_temp.sign = !op1_temp.sign;
		op2_temp = op2.left_bound;
		if (!isPosOrNegZero(op2_temp) || op2.left_open)
			op2_temp.sign = !op2_temp.sign;
		timesposright_fused(&rcan[rcan_size], op1_temp, op1.left_open, op2_temp,
				op2.left_open);
		rcan_size++;
	}

	// Lower right corner in upper left quadrant, facing downhill
	if ((isLessThanZero(op1.right_bound)
			|| (isPosOrNegZero(op1.right_bound) && op1.right_open))
			&& isGreaterThanOrEqualToZero(op2.left_bound)) {
		op1_temp = op1.right_bound;
		if (!isPosOrNegZero(op1_temp) || op1.right_open)
			op1_temp.sign = !op1_temp.sign;
		timesposleft_fused(&rcan[rcan_size], op1_temp, op1.right_open,
				op2.left_bound, op2.left_open);
		if ((mpz_sgn(rcan[rcan_size].fraction) != 0)
				|| rcan[rcan_size].ubit == 1) {
			rcan[rcan_size].sign = !rcan[rcan_size].sign;
		}
		rcan_size++;
	}

	// Upper left corner in lower right quadrant, facing downhill
	if (isGreaterThanOrEqualToZero(op1.left_bound)
			&& (isLessThanZero(op2.right_bound)
					|| (isPosOrNegZero(op2.right_bound) && op2.right_open))) {
		op2_temp = op2.right_bound;
		if (!isPosOrNegZero(op2_temp) || op2.right_open)
			op2_temp.sign = !op2_temp.sign;
		timesposleft_fused(&rcan[rcan_size], op1.left_bound, op1.left_open,
				op2_temp, op2.right_open);
		if ((mpz_sgn(rcan[rcan_size].fraction) != 0)
				|| rcan[rcan_size].ubit == 1) {
			rcan[rcan_size].sign = !rcan[rcan_size].sign;
		}
		rcan_size++;
	}

	if (lcan_size == 1 && rcan_size == 1) {
		(*result).left = lcan[0];
		(*result).right = rcan[0];
	} else {
		// find the minimum value (and the second lowest) from lcan
		int min_index = 0, second_min_index = 0, max_index = 0,
				second_max_index = 0;

		double min_value = get_float_from_fused_type(lcan[0]);
		for (i = 1; i < lcan_size; i++) {
			if (get_float_from_fused_type(lcan[i]) < min_value) {
				min_index = i;
				min_value = get_float_from_fused_type(lcan[i]);
			}

			if (min_value < get_float_from_fused_type(lcan[i])
					&& get_float_from_fused_type(lcan[i])
							< get_float_from_fused_type(
									lcan[second_min_index])) {
				second_min_index = i;
			}
		}

		// find maximum value (and the second highest) from rcan
		double max_value = get_float_from_fused_type(rcan[0]);
		for (i = 1; i < rcan_size; i++) {
			if (get_float_from_fused_type(rcan[i]) > max_value) {
				max_index = i;
				max_value = get_float_from_fused_type(rcan[i]);
			}

			if (max_value > get_float_from_fused_type(rcan[i])
					&& get_float_from_fused_type(rcan[i])
							< get_float_from_fused_type(
									rcan[second_max_index])) {
				second_max_index = i;
			}
		}

		// assign values to result
		(*result).left = lcan[min_index];
		(*result).right = rcan[max_index];

		// if the magnitude of second highest is equal to the highest and one of
		// values is exact, mark the right endpoint as closed
		if (get_float_from_fused_type(lcan[min_index])
				== get_float_from_fused_type(lcan[second_min_index])
				&& (lcan[min_index].ubit == 0
						|| lcan[second_min_index].ubit == 0)) {
			(*result).left.ubit = 0;
		}

		// if the magnitude of second lowest is equal to the lowest and one of
		// values is exact, mark the left endpoint as closed
		if (get_float_from_fused_type(rcan[max_index])
				== get_float_from_fused_type(rcan[second_max_index])
				&& (rcan[max_index].ubit == 0
						|| rcan[second_max_index].ubit == 0)) {
			(*result).right.ubit = 0;
		}
	}
	return;
}

void timesposleft_fused(fused_inter_t* result, unum_t x, bool xb, unum_t y,
		bool yb) {
	//handle the common case first
	if (!isPosOrNegZero(x) && !isPosOrNegZero(y)
			&& !((isInf(x) && xb == 0) || (isInf(y) && yb == 0))) {
		unum_mul_fused(result, x, y);
		(*result).ubit = xb | yb | (*result).ubit;
		return;
	}

	if (isPosOrNegZero(x) && xb == 0) {
		if (isInf(y) && yb == 0) {
			printf("Result is a NaN in timesposleft_fused\n");
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		}
	}

	if (isPosOrNegZero(y) && yb == 0) {
		if (isInf(x) && xb == 0) {
			printf("Result is a NaN in timesposleft_fused\n");
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		}
	}

	if (isPosOrNegZero(x) && xb == 1) {
		if (isInf(y) && yb == 0) {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		} else {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 1;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		}
	}

	if (isPosOrNegZero(y) && yb == 1) {
		if (isInf(x) && xb == 0) {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		} else {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 1;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		}
	}

	if ((isInf(x) && xb == 0) || (isInf(y) && yb == 0)) {
		//In this case the answer is _g_posinfu, but we initialize
		//to zero and set the isInf flag
		(*result).exponent = 0;
		(*result).sign = 0;
		(*result).f_size = 0;
		(*result).ubit = 0;
		mpz_init((*result).fraction);
		(*result).isInf = 1;
		return;
	}

	return;
}

void timesposright_fused(fused_inter_t* result, unum_t x, bool xb, unum_t y,
		bool yb) {

	//handle the common case first
	if (!isInf(x) && !isInf(y)
			&& !((isPosOrNegZero(x) && xb == 0)
					|| (isPosOrNegZero(y) && yb == 0))) {
		unum_mul_fused(result, x, y);
		(*result).ubit = xb | yb | (*result).ubit;
		return;
	}

	if (isInf(x) && xb == 0) {
		if (isPosOrNegZero(y) && yb == 0) {
			printf("Result is a NaN in timesposleft_fused\n");
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		}
	}

	if (isInf(y) && yb == 0) {
		if (isPosOrNegZero(x) && xb == 0) {
			printf("Result is a NaN in timesposleft_fused\n");
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		}
	}

	if (isInf(x) && xb == 1) {
		if (isPosOrNegZero(y) && yb == 0) {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 1;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		}
	}

	if (isInf(y) && yb == 1) {
		if (isPosOrNegZero(x) && xb == 0) {
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 0;
			mpz_init((*result).fraction);
			(*result).isInf = 0;
			return;
		} else {
			//In this case the answer is _g_posinfu, but we initialize
			//to zero and set the isInf flag
			(*result).exponent = 0;
			(*result).sign = 0;
			(*result).f_size = 0;
			(*result).ubit = 1;
			mpz_init((*result).fraction);
			(*result).isInf = 1;
			return;
		}
	}

	if ((isPosOrNegZero(x) && xb == 0) || (isPosOrNegZero(y) && yb == 0)) {
		(*result).exponent = 0;
		(*result).sign = 0;
		(*result).f_size = 0;
		(*result).ubit = 0;
		mpz_init((*result).fraction);
		(*result).isInf = 0;
		return;
	}

	return;
}

void unum_mul_fused(fused_inter_t* result, unum_t op1, unum_t op2) {
	int op1_exp, op2_exp;
	unsigned long long op1_frac, op2_frac;
	mpz_t temp;

	if (op1.exponent) {
		op1_exp = op1.exponent - ((1 << op1.e_size) - 1);
		op1_frac = op1.fraction | (1ULL << (op1.f_size + 1));
	} else {
		op1_exp = 2 - (1 << op1.e_size);
		op1_frac = op1.fraction;
	}

	if (op2.exponent) {
		op2_exp = op2.exponent - ((1 << op2.e_size) - 1);
		op2_frac = op2.fraction | (1ULL << (op2.f_size + 1));
	} else {
		op2_exp = 2 - (1 << op2.e_size);
		op2_frac = op2.fraction;
	}

	(*result).exponent = op1_exp + op2_exp;
	mpz_init((*result).fraction);
	mpz_init(temp);
	mpz_import((*result).fraction, 1, -1, sizeof op1_frac, 0, 0, &op1_frac);
	mpz_import(temp, 1, -1, sizeof op2_frac, 0, 0, &op2_frac);
	mpz_mul((*result).fraction, (*result).fraction, temp);
	(*result).f_size = op1.f_size + op2.f_size + 2;
	(*result).sign = op1.sign ^ op2.sign;
	(*result).ubit = op1.ubit | op2.ubit;
	mpz_clear(temp);
	(*result).isInf = 0;
	return;
}

double get_float_from_fused_type(fused_inter_t op) {
	double f;
	mpf_t frac;

	mpf_init(frac);
	mpf_set_z(frac, op.fraction);
	if (op.exponent > 0)
		mpf_mul_2exp(frac, frac, op.exponent);
	else
		mpf_div_2exp(frac, frac, -op.exponent);
	mpf_div_2exp(frac, frac, op.f_size);
	if (op.sign) {
		mpf_neg(frac, frac);
	}
	f = mpf_get_d(frac);
	mpf_clear(frac);
	return f;
}

void get_unum_for_fused_inter_t(unum_t* result,
		unsigned long long multiplied_fractions, bool ubit,
		int exponent_of_result, int fraction_bit_length,
		int result_fraction_size, int sign) {
	int trailing_zeros = 0, fraction_scale, smallnormalu_exp;
	(*result).ubit = ubit;

	// normalize the fraction and get the exponent accordingly
	if (multiplied_fractions) {
		trailing_zeros = __builtin_ctzll(multiplied_fractions);
		if (trailing_zeros > 0 && !(*result).ubit) {
			multiplied_fractions >>= trailing_zeros;
			exponent_of_result += trailing_zeros;
			fraction_bit_length -= trailing_zeros;
			trailing_zeros = 0;
		}

		if (fraction_bit_length > (_g_fsizemax + 1)) {
			// first adjust the numerator of the fraction
			multiplied_fractions >>= (fraction_bit_length - _g_fsizemax - 1);
			exponent_of_result += (fraction_bit_length - _g_fsizemax - 1);
			fraction_bit_length = _g_fsizemax + 1;
			(*result).ubit = 1;
			// next adjust the denominator of the fraction
			exponent_of_result += (_g_fsizemax - result_fraction_size);
			result_fraction_size = _g_fsizemax;
		} else {
			// in this case we only need to adjust the denominator
			exponent_of_result += (fraction_bit_length - result_fraction_size
					- 1);
			result_fraction_size = fraction_bit_length - 1;
		}
	} else {
		(*result) = _g_zero;
		return;
	}

	// result is greater than what can be expressed
	if (exponent_of_result > _g_maxexp
			|| (exponent_of_result == _g_maxexp
					&& multiplied_fractions >= _g_maxfracvalhidden)) {
		(*result) = _g_maxrealu;
		// when the answer is inexact maxrealu, a bit from the fraction can be
		// thrown out
		if (_g_fsizesize > 0) {
			(*result).fraction >>= 1;
			(*result).f_size -= 1;
		}
		(*result).sign = sign;
		(*result).ubit = true;
		return;
	}

	// result is smaller than what can be expressed
	// no need to check for fractions with _g_minexp since the minimum fraction
	// is that of _g_smallsubnormalu so there cannot be a number with _g_minexp
	// as exponent and a fraction less than the fraction of _g_smallsubnormalu
	if (exponent_of_result < _g_minexp) {
		(*result) = _g_smallsubnormalu;
		(*result).fraction--;
		(*result).sign = sign;
		(*result).ubit = true;
		return;
	}

	// if the result is subnormal
	// if the exponent is less than exponent of smallnormalu, then the result is subnormal
	// smallnormalu_exp = 1 - (pow(2, (_g_esizemax - 1)) - 1);
	smallnormalu_exp = 1 - ((1 << (_g_esizemax - 1)) - 1);
	if (exponent_of_result < smallnormalu_exp) {
		(*result).exponent = 0;
		(*result).e_size = _g_esizemax - 1;
		(*result).sign = sign;

		// fix the exponent to 1-bias and calculate the bits of the fraction
		fraction_scale = smallnormalu_exp - exponent_of_result;
		fraction_bit_length = result_fraction_size + fraction_scale;

		// set the fraction
		if (fraction_bit_length > _g_fsizemax) {
			// if result is larger than fsizemax and/or is inexact
			(*result).ubit = 1;
			// shift until it fits _g_fsizemax bits
			(*result).fraction = multiplied_fractions
					>> (fraction_bit_length - _g_fsizemax);
			(*result).f_size = _g_fsizemax - 1;
		} else {
			(*result).fraction = multiplied_fractions;
			(*result).f_size = fraction_bit_length - 1;
		}
		return;
	}

	// handling normal numbers

	// sign
	(*result).sign = sign;

	// fraction
	// reset the leading bit of the multiplied fraction (hidden bit))
	multiplied_fractions &= ~(1ULL
			<< (INTERMEDIATE_FRACTION_SIZE
					- __builtin_clzll(multiplied_fractions) - 1));
	fraction_bit_length = result_fraction_size; // this length already accounts for the hidden 1

	// set the fraction
	if (multiplied_fractions == 0 && (*result).ubit == 0) {
		(*result).fraction = 0;
		(*result).f_size = 0;
	} else {
		(*result).fraction = multiplied_fractions;
		(*result).f_size = fraction_bit_length - 1;
	}

	// exponent
	if (!exponent_of_result && !(*result).fraction && !(*result).ubit) {
		(*result).fraction = 1;
		(*result).f_size = 0;
		(*result).e_size = 0;
		(*result).exponent = 0;
		return;
	} else if (exponent_of_result == 1) {
		(*result).e_size = 0;
		(*result).exponent = 1;
	} else {
		(*result).e_size = INTERMEDIATE_EXP_SIZE
				- __builtin_clz(abs(exponent_of_result - 1));
		//(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
		(*result).exponent = exponent_of_result + ((1 << (*result).e_size) - 1);
	}

	// in case the value is bordering infinity, but shouldn't
	// if (((*result).exponent != _g_maxexpval || (*result).fraction != (_g_maxfracval - 1)) && (*result).exponent ==
	// (pow(2, ((*result).e_size + 1)) - 1) && (*result).fraction == (pow(2, ((*result).f_size + 1)) - 1) &&
	// (*result).ubit) {
	if (((*result).exponent != _g_maxexpval
			|| (*result).fraction != (_g_maxfracval - 1))
			&& (*result).exponent == ((1 << ((*result).e_size + 1)) - 1)
			&& (*result).fraction == ((1ULL << ((*result).f_size + 1)) - 1)
			&& (*result).ubit) {
		// if fraction can be increased, increase that
		if ((*result).f_size < (_g_fsizemax - 1)) {
			(*result).f_size++;
			(*result).fraction = (*result).fraction << 1;
		} else {
			(*result).e_size++;
			//(*result).exponent = exponent_of_result + (pow(2, ((*result).e_size)) - 1);
			(*result).exponent = exponent_of_result
					+ ((1 << (*result).e_size) - 1);
		}
	}

	// when the answer is inexact maxrealu, remove the bit from the fraction
	if (isPosOrNegInexactMaxreal((*result)) && _g_fsizesize > 0) {
		(*result).fraction >>= 1;
		(*result).f_size -= 1;
	}

	return;
}
