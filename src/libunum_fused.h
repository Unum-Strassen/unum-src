#ifndef LIBUNUM_FUSED_H
#define LIBUNUM_FUSED_H

typedef struct fused_inter_s {
	bool sign;
	int exponent;
	mpz_t fraction;
	bool ubit;
	int f_size;
	bool isInf;
} fused_inter_t;

typedef struct fused_inter_pair_s {
	fused_inter_t left;
	fused_inter_t right;
} fused_inter_pair_t;

/*
 * timesg for fused operations
 */
void timesg_fused(fused_inter_pair_t* result, gbound_t op1, gbound_t op2);

/*
 * Unum multiplication for fused dot product
 */
void unum_mul_fused(fused_inter_t* result, unum_t op1, unum_t op2);

/*
 * timesposleft for fused dot product
 */
void timesposleft_fused(fused_inter_t* result, unum_t x, bool xb, unum_t y,
		bool yb);

/*
 * timesposright for fused dot product
 */
void timesposright_fused(fused_inter_t* result, unum_t x, bool xb, unum_t y,
		bool yb);

/*
 * returns a double precision floating point number from a fused_inter_t struct
 */
double get_float_from_fused_type(fused_inter_t op);

/*
 * returns a unum from the given fields
 */
void get_unum_for_fused_inter_t(unum_t* result,
		unsigned long long multiplied_fractions, bool ubit,
		int exponent_of_result, int fraction_bit_length,
		int result_fraction_size, int sign);

/*
 * returns a fused_inter_t structure from a unum
 */
void get_fused_inter_t_from_unum(fused_inter_t* result, unum_t x);

#endif
