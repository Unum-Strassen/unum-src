/*
 * Performs a 16 point fft computation in single precision floating point and unums in {3,4} and {3,5}
 * FFT algorithm was obtained from http://web.mit.edu/~emin/www.old/source_code/fft/fft.c
 * Compile with gcc fft_16.c -lunum -lgmp -lm
 * Run with ./a.out
 */
#include <stdio.h>
#include <libunum.h>
#include <time.h>

#define SIN_2PI_16 0.38268343236508978
#define SIN_4PI_16 0.707106781186547460
#define SIN_6PI_16 0.923879532511286740
#define C_P_S_2PI_16 1.30656296487637660
#define C_M_S_2PI_16 0.54119610014619690
#define C_P_S_6PI_16 1.3065629648763766
#define C_M_S_6PI_16 -0.54119610014619690

void R16SRFFT(float input[16], float output[16]);
void R16SRFFT_U(unum_t input[16], ubound_t output[16]);

/*Globals*/
ubound_t SIN_2PI_16_UB, SIN_4PI_16_UB, SIN_6PI_16_UB, C_P_S_2PI_16_UB, C_M_S_2PI_16_UB, C_P_S_6PI_16_UB,
    C_M_S_6PI_16_UB;

int main()
{
    int rep1 = 100;
    int rep2 = 1000;
    int volatile x;
    int i, j, k;
    double volatile t1, t2, loop, t3, clk;
    struct timespec ts0_start, ts0_end;
    struct timespec ts1_start, ts1_end;
    struct timespec ts2_start, ts2_end;
    struct timespec ts3_start, ts3_end;
    struct timespec ts4_start, ts4_end;

    float data[16] = { 0.886162, -0.500063, -0.377491, -0.214254, 1.38067, -0.145534, 0.928555,  1.20742,
                       0.18595,  -0.976361, -0.844801, -0.488374, 1.17167, 0.660351,  -0.788365, 1.58069 };
    float output[16];
    float zero = 0;

    /*for (k = 0; k < 10; k++) {
        for (j = 0; j < rep2; j++) {
            for (i = 0; i < rep1; i++) {
                x++;
            }
            x++;
        }
    }

    ts0_start.tv_sec = ts0_start.tv_nsec = ts0_end.tv_sec = ts0_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts0_start) != 0)
        perror("gettime error");

    for (k = 0; k < 10; k++) {
        for (j = 0; j < rep2; j++) {
            for (i = 0; i < rep1; i++) {
                x++;
            }
            x++;
        }
    }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts0_end) != 0)
        perror("gettime error");

    t1 = (double)(ts0_start.tv_sec) + ((double)(ts0_start.tv_nsec) * 1e-9);
    t2 = (double)(ts0_end.tv_sec) + ((double)(ts0_end.tv_nsec) * 1e-9);
    loop = (t2 - t1) / 10.0;

    printf("loop = %lf\n", loop);
    fflush(stdout);


    ts2_start.tv_sec = ts2_start.tv_nsec = ts2_end.tv_sec = ts2_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts2_start) != 0)
        perror("gettime error");

        for (j = 0; j < rep2; j++) {
            for (i = 0; i < rep1; i++) {
                R16SRFFT(data, output);
            }
        }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts2_end) != 0)
        perror("gettime error");

    t1 = (double)(ts2_start.tv_sec) + ((double)(ts2_start.tv_nsec) * 1e-9);
    t2 = (double)(ts2_end.tv_sec) + ((double)(ts2_end.tv_nsec) * 1e-9);

    t3 = (t2 - t1);

    printf("R16SRFFT (single precision float) time = %lf (without loop = %lf)\n", t3, t3 - loop);
    fflush(stdout);

    printf("\nresult is:\n");
    printf("k,\t\tReal Part\t\tImaginary Part\n");
    printf("0\t\t%f\t\t%f\n", output[0], zero);
    printf("1\t\t%f\t\t%f\n", output[1], output[9]);
    printf("2\t\t%f\t\t%f\n", output[2], output[10]);
    printf("3\t\t%f\t\t%f\n", output[3], output[11]);
    printf("4\t\t%f\t\t%f\n", output[4], output[12]);
    printf("5\t\t%f\t\t%f\n", output[5], output[13]);
    printf("6\t\t%f\t\t%f\n", output[6], output[14]);
    printf("7\t\t%f\t\t%f\n", output[7], output[15]);
    printf("8\t\t%f\t\t%f\n", output[8], zero);
    printf("9\t\t%f\t\t%f\n", output[7], -output[15]);
    printf("10\t\t%f\t\t%f\n", output[6], -output[14]);
    printf("11\t\t%f\t\t%f\n", output[5], -output[13]);
    printf("12\t\t%f\t\t%f\n", output[4], -output[12]);
    printf("13\t\t%f\t\t%f\n", output[3], -output[11]);
    printf("14\t\t%f\t\t%f\n", output[2], -output[9]);
    printf("15\t\t%f\t\t%f\n", output[1], -output[8]);*/

    set_env(2, 4);

    unum_t data_u[16], zero_u;
    ubound_t output_u[16];
    ubound_t zero_ub;
    int count;
    unum_t SIN_2PI_16_U, SIN_4PI_16_U, SIN_6PI_16_U, C_P_S_2PI_16_U, C_M_S_2PI_16_U, C_P_S_6PI_16_U, C_M_S_6PI_16_U;

    x2u(0.0, &zero_u);
    x2u(SIN_2PI_16, &SIN_2PI_16_U);
    x2u(SIN_4PI_16, &SIN_4PI_16_U);
    x2u(SIN_6PI_16, &SIN_6PI_16_U);
    x2u(C_P_S_2PI_16, &C_P_S_2PI_16_U);
    x2u(C_M_S_2PI_16, &C_M_S_2PI_16_U);
    x2u(C_P_S_6PI_16, &C_P_S_6PI_16_U);
    x2u(C_M_S_6PI_16, &C_M_S_6PI_16_U);
    ubound_from_unum_init(&zero_ub, zero_u);
    ubound_from_unum_init(&SIN_2PI_16_UB, SIN_2PI_16_U);
    ubound_from_unum_init(&SIN_4PI_16_UB, SIN_4PI_16_U);
    ubound_from_unum_init(&SIN_6PI_16_UB, SIN_6PI_16_U);
    ubound_from_unum_init(&C_P_S_2PI_16_UB, C_P_S_2PI_16_U);
    ubound_from_unum_init(&C_M_S_2PI_16_UB, C_M_S_2PI_16_U);
    ubound_from_unum_init(&C_P_S_6PI_16_UB, C_P_S_6PI_16_U);
    ubound_from_unum_init(&C_M_S_6PI_16_UB, C_M_S_6PI_16_U);

    for (count = 0; count < 16; count++) {
        x2u(data[count], &data_u[count]);
    }

    /*ts4_start.tv_sec = ts4_start.tv_nsec = ts4_end.tv_sec = ts4_end.tv_nsec = 0;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_start) != 0)
        perror("gettime error");

    for (j = 0; j < rep2; j++) {
        for (i = 0; i < rep1; i++) {
                                R16SRFFT_U(data_u, output_u);
                }
        }

    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts4_end) != 0)
        perror("gettime error");

    t1 = (double)(ts4_start.tv_sec) + ((double)(ts4_start.tv_nsec) * 1e-9);
    t2 = (double)(ts4_end.tv_sec) + ((double)(ts4_end.tv_nsec) * 1e-9);
    t3 = t2 - t1;

    printf("R16SRFFT_U time = %lf (without loop = %lf)\n", t3, t3 - loop);*/

    unum_reset_bit_track_counts();

    /*for(i = 0; i < 16; i++) {
                unumview(data_u[i]);
                printf("\n");
    }

    printf("\n");
    printf("\n");*/

    R16SRFFT_U(data_u, output_u);

    for (i = 0; i < 16; i++) {
        uboundview(output_u[i]);
        printf("\n");
    }

    printf("total number of bits = %llu\n", total_bit_count);
    printf("total number of operands = %llu\n", total_op_count);
    printf("maximum number of bits = %d\n", max_bit_count);
    printf("minimum number of bits = %d\n", min_bit_count);
    */

        /*printf("\n");
        printf("\n");

        printf("\nunum result is:\n");
        printf("k=0\t\t\nReal Part\n");
        uboundview(output_u[0]);
        printf("\nImaginary Part\n");
        uboundview(zero_ub);
        printf("\n\nk=1\t\t\nReal Part\n");
        uboundview(output_u[1]);
        printf("\nImaginary Part\n");
        uboundview(output_u[9]);
        printf("\n\nk=2\t\t\nReal Part\n");
        uboundview(output_u[2]);
        printf("\nImaginary Part\n");
        uboundview(output_u[10]);
        printf("\n\nk=3\t\t\nReal Part\n");
        uboundview(output_u[3]);
        printf("\nImaginary Part\n");
        uboundview(output_u[11]);
        printf("\n\nk=4\t\t\nReal Part\n");
        uboundview(output_u[4]);
        printf("\nImaginary Part\n");
        uboundview(output_u[12]);
        printf("\n\nk=5\t\t\nReal Part\n");
        uboundview(output_u[5]);
        printf("\nImaginary Part\n");
        uboundview(output_u[13]);
        printf("\n\nk=6\t\t\nReal Part\n");
        uboundview(output_u[6]);
        printf("\nImaginary Part\n");
        uboundview(output_u[14]);
        printf("\n\nk=7\t\t\nReal Part\n");
        uboundview(output_u[7]);
        printf("\nImaginary Part\n");
        uboundview(output_u[15]);
        printf("\n\nk=8\t\t\nReal Part\n");
        uboundview(output_u[8]);
        printf("\nImaginary Part\n");
        uboundview(zero_ub);
        negate_ubound(&output_u[15]);
        printf("\n\nk=9\t\t\nReal Part\n");
        uboundview(output_u[7]);
        printf("\nImaginary Part\n");
        uboundview(output_u[15]);
        negate_ubound(&output_u[14]);
        printf("\n\nk=10\t\t\nReal Part\n");
        uboundview(output_u[6]);
        printf("\nImaginary Part\n");
        uboundview(output_u[14]);
        negate_ubound(&output_u[13]);
        printf("\n\nk=11\t\t\nReal Part\n");
        uboundview(output_u[5]);
        printf("\nImaginary Part\n");
        uboundview(output_u[13]);
        negate_ubound(&output_u[12]);
        printf("\n\nk=12\t\t\nReal Part\n");
        uboundview(output_u[4]);
        printf("\nImaginary Part\n");
        uboundview(output_u[12]);
        negate_ubound(&output_u[11]);
        printf("\n\nk=13\t\t\nReal Part\n");
        uboundview(output_u[3]);
        printf("\nImaginary Part\n");
        uboundview(output_u[11]);
        negate_ubound(&output_u[9]);
        printf("\n\nk=14\t\t\nReal Part\n");
        uboundview(output_u[2]);
        printf("\nImaginary Part\n");
        uboundview(output_u[9]);
        negate_ubound(&output_u[8]);
        printf("\n\nk=15\t\t\nReal Part\n");
        uboundview(output_u[1]);
        printf("\nImaginary Part\n");
        uboundview(output_u[8]);*/
        printf("\n");

    return 0;
}

void R16SRFFT_U(unum_t input[16], ubound_t output[16])
{
    ubound_t temp, out0, out1, out2, out3, out4, out5, out6, out7, out8;
    ubound_t out9, out10, out11, out12, out13, out14, out15;

    plusu(&out0, input[0], input[8]);
    plusu(&out1, input[1], input[9]);
    plusu(&out2, input[2], input[10]);
    plusu(&out3, input[3], input[11]);
    plusu(&out4, input[4], input[12]);
    plusu(&out5, input[5], input[13]);
    plusu(&out6, input[6], input[14]);
    plusu(&out7, input[7], input[15]);

    minusu(&out8, input[0], input[8]);
    minusu(&out9, input[1], input[9]);
    minusu(&out10, input[2], input[10]);
    minusu(&out11, input[3], input[11]);
    minusu(&out12, input[12], input[4]);
    minusu(&out13, input[13], input[5]);
    minusu(&out14, input[14], input[6]);
    minusu(&out15, input[15], input[7]);

    minusubound(&temp, out13, out9);
    timesubound(&temp, temp, SIN_2PI_16_UB);
    timesubound(&out9, out9, C_P_S_2PI_16_UB);
    plusubound(&out9, out9, temp);
    timesubound(&out13, out13, C_M_S_2PI_16_UB);
    plusubound(&out13, out13, temp);

    timesubound(&out14, out14, SIN_4PI_16_UB);
    timesubound(&out10, out10, SIN_4PI_16_UB);
    minusubound(&out14, out14, out10);
    plusubound(&out10, out10, out10);
    plusubound(&out10, out14, out10);

    minusubound(&temp, out15, out11);
    timesubound(&temp, temp, SIN_6PI_16_UB);
    timesubound(&out11, out11, C_P_S_6PI_16_UB);
    plusubound(&out11, out11, temp);
    timesubound(&out15, out15, C_M_S_6PI_16_UB);
    plusubound(&out15, out15, temp);

    plusubound(&out8, out8, out10);
    temp = out10;
    minusubound(&out10, out8, out10);
    minusubound(&out10, out10, temp);

    plusubound(&out12, out12, out14);
    temp = out14;
    minusubound(&out14, out12, out14);
    minusubound(&out14, out14, temp);

    plusubound(&out9, out9, out11);
    temp = out11;
    minusubound(&out11, out9, out11);
    minusubound(&out11, out11, temp);

    plusubound(&out13, out13, out15);
    temp = out15;
    minusubound(&out15, out13, out15);
    minusubound(&out15, out15, temp);

    /*The following are the final set of two point butterflies */
    plusubound(&output[1], out8, out9);
    minusubound(&output[7], out8, out9);

    plusubound(&output[9], out12, out13);
    minusubound(&output[15], out13, out12);

    plusubound(&output[5], out10, out15);
    minusubound(&output[13], out14, out11);
    minusubound(&output[3], out10, out15);
    negate_ubound(&out14);
    minusubound(&output[11], out14, out11);

    /* First set of 2-point butterflies */
    plusubound(&out0, out0, out4);
    temp = out4;
    minusubound(&out4, out0, out4);
    minusubound(&out4, out4, temp);
    plusubound(&out1, out1, out5);
    temp = out5;
    minusubound(&out5, out1, out5);
    minusubound(&out5, out5, temp);
    plusubound(&out2, out2, out6);
    temp = out6;
    minusubound(&out6, out2, out6);
    minusubound(&out6, out6, temp);
    plusubound(&out3, out3, out7);
    temp = out7;
    minusubound(&out7, out3, out7);
    minusubound(&out7, out7, temp);

    /* Computations to find X[0], X[4], X[6] */
    plusubound(&output[0], out0, out2);
    minusubound(&output[4], out0, out2);
    plusubound(&out1, out1, out3);
    plusubound(&output[12], out3, out3);
    minusubound(&output[12], output[12], out1);

    plusubound(&output[0], output[0], out1);
    temp = out1;
    minusubound(&output[8], output[0], out1);
    minusubound(&output[8], output[8], temp);

    /* Computations to find X[5], X[7] */
    timesubound(&out5, out5, SIN_4PI_16_UB);
    timesubound(&out7, out7, SIN_4PI_16_UB);
    minusubound(&out5, out5, out7);
    plusubound(&out7, out7, out7);
    plusubound(&out7, out7, out5);

    minusubound(&output[14], out6, out7);
    plusubound(&output[2], out5, out4);
    minusubound(&output[6], out4, out5);
    negate_ubound(&out7);
    minusubound(&output[10], out7, out6);

    return;
}

void R16SRFFT(float input[16], float output[16])
{
    float temp, out0, out1, out2, out3, out4, out5, out6, out7, out8;
    float out9, out10, out11, out12, out13, out14, out15;

    out0 = input[0] + input[8]; /* output[0 through 7] is the data that we */
    out1 = input[1] + input[9]; /* take the 8 point real FFT of. */
    out2 = input[2] + input[10];
    out3 = input[3] + input[11];
    out4 = input[4] + input[12];
    out5 = input[5] + input[13];
    out6 = input[6] + input[14];
    out7 = input[7] + input[15];

    out8 = input[0] - input[8];   /* inputs 8,9,10,11 are */
    out9 = input[1] - input[9];   /* the Real part of the */
    out10 = input[2] - input[10]; /* 4 point Complex FFT inputs.*/
    out11 = input[3] - input[11];
    out12 = input[12] - input[4]; /* outputs 12,13,14,15 are */
    out13 = input[13] - input[5]; /* the Imaginary pars of  */
    out14 = input[14] - input[6]; /* the 4 point Complex FFT inputs.*/
    out15 = input[15] - input[7];

    /*First we do the "twiddle factor" multiplies for the 4 point CFFT */
    /*Note that we use the following handy trick for doing a complex */
    /*multiply:  (e+jf)=(a+jb)*(c+jd) */
    /*   e=(a-b)*d + a*(c-d)   and    f=(a-b)*d + b*(c+d)  */

    /* C_M_S_2PI/16=cos(2pi/16)-sin(2pi/16) when replaced by macroexpansion */
    /* C_P_S_2PI/16=cos(2pi/16)+sin(2pi/16) when replaced by macroexpansion */
    /* (SIN_2PI_16)=sin(2pi/16) when replaced by macroexpansion */
    temp = (out13 - out9) * (SIN_2PI_16);
    out9 = out9 * (C_P_S_2PI_16) + temp;
    out13 = out13 * (C_M_S_2PI_16) + temp;

    out14 *= (SIN_4PI_16);
    out10 *= (SIN_4PI_16);
    out14 = out14 - out10;
    out10 = out14 + out10 + out10;

    temp = (out15 - out11) * (SIN_6PI_16);
    out11 = out11 * (C_P_S_6PI_16) + temp;
    out15 = out15 * (C_M_S_6PI_16) + temp;

    /* The following are the first set of two point butterfiles */
    /* for the 4 point CFFT */

    out8 += out10;
    out10 = out8 - out10 - out10;

    out12 += out14;
    out14 = out12 - out14 - out14;

    out9 += out11;
    out11 = out9 - out11 - out11;

    out13 += out15;
    out15 = out13 - out15 - out15;

    /*The followin are the final set of two point butterflies */
    output[1] = out8 + out9;
    output[7] = out8 - out9;

    output[9] = out12 + out13;
    output[15] = out13 - out12;

    output[5] = out10 + out15;   /* implicit multiplies by */
    output[13] = out14 - out11;  /* a twiddle factor of -j */
    output[3] = out10 - out15;   /* implicit multiplies by */
    output[11] = -out14 - out11; /* a twiddle factor of -j */

    /* What follows is the 8-point FFT of points output[0-7] */
    /* This 8-point FFT is basically a Decimation in Frequency FFT */
    /* where we take advantage of the fact that the initial data is real*/

    /* First set of 2-point butterflies */

    out0 = out0 + out4;
    out4 = out0 - out4 - out4;
    out1 = out1 + out5;
    out5 = out1 - out5 - out5;
    out2 += out6;
    out6 = out2 - out6 - out6;
    out3 += out7;
    out7 = out3 - out7 - out7;

    /* Computations to find X[0], X[4], X[6] */

    output[0] = out0 + out2;
    output[4] = out0 - out2;
    out1 += out3;
    output[12] = out3 + out3 - out1;

    output[0] += out1;                   /* Real Part of X[0] */
    output[8] = output[0] - out1 - out1; /*Real Part of X[4] */
    /* out2 = Real Part of X[6] */
    /* out3 = Imag Part of X[6] */

    /* Computations to find X[5], X[7] */

    out5 *= SIN_4PI_16;
    out7 *= SIN_4PI_16;
    out5 = out5 - out7;
    out7 = out5 + out7 + out7;

    output[14] = out6 - out7;  /* Imag Part of X[5] */
    output[2] = out5 + out4;   /* Real Part of X[7] */
    output[6] = out4 - out5;   /*Real Part of X[5] */
    output[10] = -out7 - out6; /* Imag Part of X[7] */
}
