#Unums in C

##Introduction
This is a C implementation of Unum arithmetic introduced by John L. Gustafson in his book "[The End of Error](https://www.crcpress.com/The-End-of-Error-Unum-Computing/Gustafson/p/book/9781482239867)". 
Our goal is to provide a set of fast software routines for Unum computation in order to promote the wider use and experimentaiton with Unums.

##Installation

Download the archive or clone the project, extract and cd into its /src directory. Next execute the below commands.

1.make (If unification is requried execute "make UNIFY=1")

2.sudo make install

##Usage

Include <libunum.h> and link with -lunum during compilation. (-lgmp maybe required)

*Currently supports environments upto {4,5}